//
//  CO_PriorityQueue.cpp
//  CO With IDs
//
//  Created by Karl Gemayel on 05/03/12.
//  Modifed by Karl Gemayel on 05/03/12.
//  Copyright 2012 American University of Beirut. All rights reserved.

/*
 *	Specifications:
 *		1) Funnel Heap-based implementation of a (max) priority queue.
 *		2) Deals with polynomials whose monomials have an ID packed in them.
 *		3) Does not do any merging in the sweep and insertInI functions. This 
 *			is because we can't merge two monomials that have different IDs.
 *
 */



/********************************** MEMORY LAYOUT **********************************\
 *          ________________________________________________________
 * QUEUE:  | Queue Size | I | Link 1 | Link 2 | ... | Link i | ... |     
 *          -------------------------------------------------------
 *          ___________________________________________________________________
 * Link i: | Capacity | L_i | c_i | r_i | A_i | B_i | K_i | S_i1 | ... | S_iki |
 *          -------------------------------------------------------------------
 *         |_____________ ______________|
 *                       |
 *                 Link Overhead
 *
 *                                  ____________________________________________________
 * I, A_i, B_i, S_i1...S_iki:      | P_i | H_i | Left | Right | Parent | HT |  elements |
 *                                  ----------------------------------------------------
 *                                 |___________________ ____________________|
 *                                                     |
 *                                              Buffer Overhead
 *
 * NOTES:
 *  1) The link Capacity is the number of elements that can fit from c_i to S_iki
 *  2) P_i contains the size, capacity, isLeaf, and isExhausted bool of a buffer (details below)
 *  3) H_i contains the height of the tree, and the index of the buffer (details below)
 *  4) L_i contains the current link index, and the total number of links (details below)
 *  5) HT contains the the indices of the head and tail of the elements
 *  6) r_i represents the number of elements in the lower part of the link (ie B_i till leafs)
 *  7) Buffer Index is the index of the buffer:
 *          i) Index of I is the max number that can fit
 *         ii) Index of A_i is 0
 *        iii) Indices of B_i to S_ik (including K_i) are defined as follows:
 *                  - B_i is 1
 *                  - S_ik is the number of buffers from B_i to S_ik (including K_i)
 *                  - The rest are in sequential order, from left to right, by level.
 *
 * General Info:
 *  1) Links are indexed starting from 1
 *  2) Leafs are indexed starting from 1
 *  3) The root of every K-merger (the B buffer) is at height 0
 *
 ***********************************************************************************/


#include <iostream>
#include <cstdlib>
#include <math.h>
#include <cstdio>
#include <stack>
#include <queue>
#include "FunnelHeap.h"

#include <assert.h>

using namespace std;



// Enable assertions to ensure that the functions are operating correctly on a technical level
#define ASSERTIONS false

// Enable the PROFILE_ADAPTIVE_PERF to optimize the insertion (when a sweep is needed)
#define PROFILE_ADAPTIVE_PERF false

bool temp_assertions = false;

// This variable is a NULL type for heap_t iterators
heap_t::iterator I_NULL;

// variables for debugging
bool check_for_prob = false;
monom_t just_counter = 0;
heap_t::iterator prob_buffer = I_NULL;
void testLink3();

// type of queue used for streams 
typedef queue<monom_t> stream_t;


// Link Overhead Elements
#define LINK_OVERHEAD (int) 4       // number of elements in beginning of a link or
#define LINK_CAP 0                  // Capacity of the current link
#define TI_INDEX 1                  // TI: Total number of links, link index
#define EMPTY_LEAF_COUNTER_INDEX 2  // The counter indicating an empty leaf (used in sweep)
#define NUMBER_OF_ELEMENTS_INDEX 3  // The number of elements in the lower part of a link


// Buffer Overhead Elements
#define BUFFER_OVERHEAD (int) 7     // overhead amount associated with each buffer
#define SCLE_INDEX 0                // SCLE: Size, capacity, isLeaf, isExhausted of buffer
#define HI_INDEX 1                  // HI: Height of K-merger, index of buffer
#define LEFT_CHILD_INDEX 2          // Distance to left child (in memory) from current position
#define RIGHT_CHILD_INDEX 3         // Distance to right child (in memory) from current position
#define PARENT_INDEX 4              // Distance to parent (in memory) from current position
#define HT_INDEX 5                  // HT: Index to head and tail of elements in the buffer
#define BUFFER_TYPE_INDEX 6         // The type of the buffer: I, A, B, Interior, or Leaf



#define I_INDEX 4294967295   //(long) pow(2, 31*8) - 1    // computes the index of the buffer I


// FOR DEBUGGING
heap_t myQueue;

#pragma mark -
#pragma mark Function Prototypes

/* Function Prototypes */

// poll
monom_t private_poll (heap_t::iterator qBegin);
monom_t fakePoll (heap_t::iterator qBegin);

// setting default queue values
void setQueueDefaultValues(heap_t &queue, long numOfLink);
void setKMergerDefaultValues (heap_t &k_merger, long k, long outputIndex, const long capacityPerLevel[], long currentLevel, const long height);
long updateKCapacity (long k, heap_t::iterator &mem, heap_t::iterator parent_mem);

// computations of sizes and capacities
void getKAndS (long values [], int i);
unsigned long long computeLinkCapacity(int i);
int kSize(long k);

// traversing the queue
heap_t::iterator gotoLink(heap_t::iterator queue, int i);
heap_t::iterator nextNodeAtHeight (heap_t::iterator leaf, long height, stack<char> &state);
heap_t::iterator nextNodeAtDepth (heap_t::iterator current, long depth, stack<char> &state);  // USE THIS
int getLeafsForPAP(heap_t::iterator link_i, heap_t::iterator &leaf_1, heap_t::iterator &leaf_2);

// inserting, deleting, merging...
void mergeStep (heap_t::iterator parent);
void pushOnTail (heap_t::iterator buffer, monom_t element);
monom_t popMax (heap_t::iterator buffer);
void fill (heap_t::iterator buffer);

// Sweeping
void sweep (heap_t::iterator q, int linkIndex);
void mergeStreams (stream_t& stream1, stream_t& stream2, stream_t& output);
void formStream1 (heap_t::iterator A_i, monom_t c_i, long linkIndex, stream_t& stream1, queue<long> &pathSizes);
void formStream2 (heap_t::iterator q, long linkIndex, stream_t& stream2);
void insertStreamToPath (heap_t::iterator A_1, stream_t& stream1, stream_t& stream2, long A_sizes[], queue<long> &partOfPSizes, monom_t c_i, long linkIndex);

void getPath(monom_t c, long height, stack<char> &path);
//void insertInBufferWithoutIncSize (heap_t::iterator buffer, monom_t element);
void insertInBuffer (heap_t::iterator buffer, monom_t element);
void insertInI (heap_t::iterator I, deg_t degree, coef_t coef, ID_t f_id);

// for packing and unpacking
monom_t packSCLE (monom_t size, monom_t capacity, monom_t leaf, monom_t exhausted);
void unpackSCLE (monom_t sce, long output []);

monom_t packHI (long height, long bufferIndex);
void unpackHI (monom_t hi, long output []);

monom_t packTI (long totalLinks, long currentIndex);
void unpackTI (monom_t ti, long output []);

monom_t packLR (long leftChild, long rightChild);
void unpackLR (monom_t lr, long output []);

monom_t packHT (long head, long tail);
void unpackHT (monom_t ht, long output []);



// setting/accessing parent children
bool isLeaf (heap_t::iterator currentBuffer);
void setLeftChild (heap_t::iterator buffer, monom_t left);
void setRightChild (heap_t::iterator buffer, monom_t right);
void setParent (heap_t::iterator buffer, monom_t parent);
void setLeftRightParent (heap_t::iterator buffer, monom_t left, monom_t right, monom_t parent);

heap_t::iterator getLeftChild (heap_t::iterator parent);
heap_t::iterator getRightChild (heap_t::iterator parent);
heap_t::iterator getParent (heap_t::iterator child);


// setting/accessing buffer attributes
inline void setBufferSize (heap_t::iterator buffer, monom_t size);
inline void setBufferCapacity (heap_t::iterator buffer, monom_t capacity);
inline void setExhausted (heap_t::iterator buffer, bool isExhausted);
inline void setEmptyLeafCounter (heap_t::iterator link_i, monom_t c_i);
inline void setNumberOfElementsInLower(heap_t::iterator link_i, monom_t number);
inline void setBufferType(heap_t::iterator buffer, int type);

inline long getBufferSize (heap_t::iterator buffer);
inline long getBufferCapacity (heap_t::iterator buffer);
inline bool isExhausted (heap_t::iterator buffer);
inline monom_t getEmptyLeafCounter (heap_t::iterator link_i);
inline monom_t getNumberOfElementsInLower (heap_t::iterator link_i);
inline int getBufferType (heap_t::iterator buffer);

inline bool isBufferEmpty(heap_t::iterator buffer);
inline bool isBufferFull(heap_t::iterator buffer);

inline void incrementNumberOfElementsInLowerBy (heap_t::iterator link_i, monom_t increment);
inline void decrementNumberOfElementsInLowerBy (heap_t::iterator link_i, monom_t decrement);

inline monom_t getMax (heap_t::iterator buffer);


// buffer elements manipulation
void nextElement (long &pos, long size);
void previousElement (long &pos, long size);
void newHeadIndex (long &head, long size, long capacity);
void newTailIndex (long &tail, long size, long capacity);




// TESTING
bool testPackingAndUnpacking (bool printLog);
bool test_I_Traversing (heap_t::iterator I, bool printLog);

heap_t::iterator getNodeAtLevel (heap_t::iterator B_i, int nodePos, long level);
void testTreeCapacities (heap_t::iterator B_i, long linkIndex);

// PRINTING STATES
void printBuffer(heap_t::iterator buffer);
void printKMerger (heap_t::iterator B_i, long height);
void printLeafs (heap_t::iterator B_i, long linkIndex);
void printBufferElements (heap_t::iterator buffer);
void printLink(heap_t::iterator link);
void printKMergerElements (heap_t::iterator B_i, long height);
void printLeafElements (heap_t::iterator B_i, long height);
void printQueueElements (heap_t::iterator queue);
void printQueueElements(heap_t::iterator q);
void printQueueState();



#pragma mark -
#pragma mark Packing and Unpacking SCLE

/********* Packing and Unpacking: Size, Capacity, isLeaf, and isExhausted boolean ********\
 *                   ________________________________________
 * Structure of P:  | Size | Capacity | isLeaf | isExhausted |
 *                   ----------------------------------------
 *                      4       3         2          1
 *
 * How P is divided:
 *      1) isExhausted  : takes up only 1 bit
 *      2) isLeaf       : takes up only 1 bit
 *      3) Capacity     : takes up (sizeof(word)-1)/2 bits (ceiling)
 *      4) Size         : takes up (sizeof(word)-1)/2 bits (floor)
 */



#define SIZE_SHIFT 33      // to shift the size value x positions to the left
#define CAP_SHIFT  2       // to shift the capacity value 2 position to the left
#define LEAF_SHIFT 1       // to shift the isLeaf value 1 position to the left

#define SIZE_MASK   0xFFFFFFFE00000000ULL
#define CAP_MASK    0x00000001FFFFFFFCULL
#define LEAF_MASK   0x0000000000000002ULL
#define EXH_MASK    0x0000000000000001ULL

#define ZERO_SIZE   0x00000001FFFFFFFFULL
#define ZERO_CAP    0xFFFFFFFE00000003ULL
#define ZERO_LEAF   0xFFFFFFFFFFFFFFFDULL
#define ZERO_EXH    0xFFFFFFFFFFFFFFFEULL

#define UNPACK_SIZE(W)      ((long) ((W & SIZE_MASK) >> SIZE_SHIFT))
#define UNPACK_CAPACITY(W)  ((long) ((W & CAP_MASK) >> CAP_SHIFT))
#define UNPACK_IS_LEAF(W)   ((long) ((W & LEAF_MASK) >> LEAF_SHIFT))
#define UNPACK_EXHAUSTED(W) ((long) (W & EXH_MASK))



/*
 * packSCLE: packs the size, capacity, and exhausted values into a single
 *          word of type monom_t
 */
monom_t packSCLE (monom_t size, monom_t capacity, monom_t leaf, monom_t exhausted) {
    monom_t word = 0;
    
    word = size;
    word = word << SIZE_SHIFT;
    word = word | (( capacity) << CAP_SHIFT);
    
    if (leaf != 0) {
        word = word | (leaf << LEAF_SHIFT);
    }
    
    if (exhausted != 0) {
        word = word | EXH_MASK;
    }
    
    return word;    
}


/*
 * unpackSCLE: unpacks the size, capacity, and exhausted values from the word
 *            and places them in that order in the output array
 */
void unpackSCLE (monom_t scle, long output []) {    
    long exhausted = UNPACK_EXHAUSTED(scle);
    long isLeaf = UNPACK_IS_LEAF(scle);
    long capacity = (long) UNPACK_CAPACITY(scle);
    long size = UNPACK_SIZE(scle);
    
    output [0] = exhausted;
    output [1] = isLeaf;
    output [2] = capacity;
    output [3] = size;
}


/******** End of Packing and Unpacking SCLE ********/




#pragma mark -
#pragma mark Packing and Unpacking HI

/********* Packing and Unpacking: Height, Buffer index ********\
 *                   _______________________
 * Structure of H:  | Height | Buffer Index |
 *                   -----------------------
 *                      2           1
 *
 * How H is divided:
 *      1) Buffer Index: takes up (sizeof(word)-1)/2 (ceiling)
 *      2) Height : takes up sizeof(word) - sizeof(buffer index)
 */

#define H_SHIFT 32       // to shift the capacity value 1 position to the left

#define H_MASK   0xFFFFFFFF00000000ULL
#define BI_MASK  0x00000000FFFFFFFFULL

#define UNPACK_HEIGHT(W)        ((long) ((W & H_MASK) >> H_SHIFT))
#define UNPACK_BUFFER_INDEX(W)  ((long) ((W & BI_MASK)))


/*
 * packHI: packs the height and buffer index values into a single
 *          word of type monom_t
 */
monom_t packHI (long height, long bufferIndex) {
    monom_t word = 0;
    
    word = height;
    word = word << H_SHIFT;
    word = (word | bufferIndex);
    
    return word;    
}


/*
 * unpackHI: unpacks the height and buffer index values from the word
 *            and places them in that order in the output array
 */
void unpackHI (monom_t hi, long output []) {
    long height = UNPACK_HEIGHT(hi);
    long bufferIndex = UNPACK_BUFFER_INDEX(hi);
    
    
    output [0] = height;
    output [1] = bufferIndex;
    
}


/******** End of Packing and Unpacking HI ********/




#pragma mark -
#pragma mark Packing and Unpacking TI

/********* Packing and Unpacking: Total links, Link Index ********\ 
 *                   ______________________________
 * Structure of L:  | Number of Links | Link Index |
 *                   ------------------------------
 *                          2               1
 *
 * How L is divided:
 *      1) Link Index: takes up (sizeof(word)-1)/2 (ceiling)
 *      2) Number of Links: takes up sizeof(word) - sizeof(buffer index)
 */

#define T_SHIFT 32       // to shift the capacity value 1 position to the left

#define T_MASK   0xFFFFFFFF00000000ULL          // total number of links mask
#define LI_MASK  0x00000000FFFFFFFFULL         // link index mask

#define UNPACK_TOTAL_LINKS(W)       ((long) ((W & H_MASK) >> H_SHIFT))
#define UNPACK_LINK_INDEX(W)        ((long) ((W & LI_MASK)))


/*
 * packTI: packs the total number of links and link index values into a single
 *          word of type monom_t
 */
monom_t packTI (long numOfLinks, long linkIndex) {
    monom_t word = 0;
    
    word = numOfLinks;
    word = word << T_SHIFT;
    word = (word | linkIndex);
    
    return word;    
}


/*
 * unpackTI: unpacks the total number of links and link index values from the word
 *            and places them in that order in the output array
 */
void unpackTI (monom_t ti, long output []) {
    long numOfLinks = UNPACK_TOTAL_LINKS(ti);
    long linkIndex = UNPACK_LINK_INDEX(ti);
    
    
    output [0] = numOfLinks;
    output [1] = linkIndex;
    
}


/******** End of Packing and Unpacking TI ********/



#pragma mark -
#pragma mark Packing and Unpacking LR

/********* Packing and Unpacking: Left child, Right child ********/






/******** End of Packing and Unpacking LRP ********/



#pragma mark -
#pragma mark Packing and Unpacking HT

/********* Packing and Unpacking: Head and Tail **********
 *                   ______________
 * Structure of HT:  | Head | Tail |
 *                   --------------
 *                      2      1
 *
 * How HT is divided:
 *      1) Tail: takes up (sizeof(word)-1)/2 (ceiling)
 *      2) Head: takes up sizeof(word) - sizeof(Tail)
 */
 

#define HEAD_SHIFT 32       // to shift the capacity value 1 position to the left

#define HEAD_MASK 0xFFFFFFFF00000000ULL          // total number of links mask
#define TAIL_MASK 0x00000000FFFFFFFFULL         // link index mask

#define UNPACK_HEAD(W)       ((long) ((W & HEAD_MASK) >> HEAD_SHIFT))
#define UNPACK_TAIL(W)       ((long) ((W & TAIL_MASK)))


/*
 * packHT: packs the total number of links and link index values into a single
 *          word of type monom_t
 */
monom_t packHT (long head, long tail) {
    monom_t word = 0;
    
    word = head;
    word = word << T_SHIFT;
    word = (word | tail);
    
    return word;    
}


/*
 * unpackHT: unpacks the total number of links and link index values from the word
 *            and places them in that order in the output array
 */
void unpackHT (monom_t ht, long output []) {
    long head = UNPACK_HEAD(ht);
    long tail = UNPACK_TAIL(ht);
    
    
    output [0] = head;
    output [1] = tail;
    
}


/********* End of Packing and Unpacking HT *********/


#pragma mark -
#pragma mark Buffer Types

/*
 * Buffers can be one of the following
 *      1) I
 *      2) A
 *      3) B
 *      4) Interior (i.e. in the K-merger)
 *      5) Leaf
 */
#define I_TYPE 0 
#define A_TYPE 1
#define B_TYPE 2
#define INTERIOR_TYPE 3
#define LEAF_TYPE 4

/*
 * setBufferType: sets the type of the buffer
 */
void setBufferType(heap_t::iterator buffer, int type) {
    (*(buffer + BUFFER_TYPE_INDEX)) = type;
}

/*
 * getBufferType: gets the type of the buffer
 */
int getBufferType(heap_t::iterator buffer) {
    return (int) (*(buffer + BUFFER_TYPE_INDEX));
}



#pragma mark -
#pragma mark Header Functions

/*
 * FunnelHeap: Allocates memory for the queue, and returns a pointer to 
 *                  its head element
 */
FunnelHeap::FunnelHeap (int numberOfLinks) {  // "numOfLinks" should be derived depending on input size (DO LATER)
	numOfLinks = numberOfLinks;
	
    const int s1 = 8;
    
    /* remark: the value of numOfLinks might start to cause a memory allocation error
     *           with value above 7 since the size of queue would be greater than 10^15
     */
    
    
    int I_size = s1 + BUFFER_OVERHEAD;       // size of I is s1 elements and the buffer overhead
    
    unsigned long long queueSize = I_size + 2;		// size of buffer I + 2 (for size of queue and I_NULL element)
    
    for (int i = 1; i <= numOfLinks; i++) {
        queueSize += computeLinkCapacity(i) + LINK_OVERHEAD;
    }
    
   /* monom_t * queue = new monom_t [queueSize]; */        /* NOTE: Queue is initialized on heap. Might be faster to use
                                                         *          if it was initialized on runtime stack
                                                         */
    
#ifdef VERBOSE
	// print queuesize
	printf("Queue size (in MB) = %llu\n", (monom_t) ((queueSize / ( 1024.0 * 1024.0) * sizeof(monom_t) )));
#endif
														
	// initialize heap
	heap.reserve(queueSize);
	
	// setup the NULL value for iterators
	I_NULL = heap.begin() + queueSize - 1;
	
    // set initial queue values
    setQueueDefaultValues(heap, numOfLinks);     

	// set a global pointer to be able to access the heap anytime, anywhere (for debugging)
    myQueue = heap;

}


bool FunnelHeap::isEmpty() {
    // return true if I is empty and A_1 is exhausted
	heap_t::iterator q_size = heap.begin();
	return ((*q_size) == 0);
}


monom_t FunnelHeap::peek() {
    heap_t::iterator I = heap.begin() + 1;
    heap_t::iterator A_1 = getLeftChild(I);
    
    if (isBufferEmpty(A_1) && !isExhausted(A_1)) {
        fill(A_1);
    }
    
	bool I_empty = isBufferEmpty(I);
	bool A_1_empty = isBufferEmpty(A_1);;
	
	// if both buffers are empty, return 0 (ie nothing)
    if (I_empty && A_1_empty) {
        return 0;
    }
    
	// if neither is empty, then take the max element from both
    if (!I_empty && !A_1_empty) {
		if (GET_DEGREE(getMax(I)) > GET_DEGREE(getMax(A_1))) {		// if I contains the max element
        	return getMax(I);
    	}
		else 		// if A_1 contains the max element
		    return getMax(A_1);
	}
	// if I is empty but A_1 isn't, take max from A_1
	else if (I_empty && !A_1_empty) {
		return getMax(A_1);
	}
	// if A_1 is empty but I isn't, take max from I
	else
		return getMax(I);
  
}


void FunnelHeap::insert (deg_t degree, coef_t coef, ID_t f_id) {
    
    /*
     * General Code Sketch:
     *
     *  1)  Start by inserting the new element in the buffer I.
     *  2)  If I becomes full, then sweep to free up space for
     *      new elements to be inserted in the queue
     */
	heap_t::iterator I = heap.begin() + 1;
	
    // insert new element in I
    insertInI(I, degree, coef, f_id);
    
	// increment the size of the queue
	heap_t::iterator q_size = heap.begin();
	(*q_size) = (*q_size) + 1;
    
    // if I is full, then return. Otherwise, move on to the sweeping
    if (!isBufferFull(I)) {
        return;
    }
    
    // locate empty leaf to sweep to
    
    // get first empty leaf
    long c_i = 0;   
    
    
    heap_t::iterator link_i = gotoLink(heap.begin(), 1);
    heap_t::iterator A_i;
    
    monom_t r_i = 0;
    long kAndS [2];
    
    long pap_ci = -1;     // profile adaptive performance c_i
    
    
    // We sweep in order to free up space in the top of the queue so
    // that we could insert new elements. This is done by shifting
    // elements further down into the queue while maintaining their
    // sorted order, thus freeing up space at the top.
    
    // The buffer to which we sweep is selected based on one of two conditions:
    //      1)  if c_i <= the number of leafs in a link i
    //      2)  if the number of elements in the lower part of a link is
    //          less than or equal to (k_i * s_i)/2
    int i = 1;
    while (true) {
        c_i = getEmptyLeafCounter(link_i);
        r_i = getNumberOfElementsInLower(link_i);
        
        
        getKAndS(kAndS, i);
        long k_i = kAndS[0];
        long s_i = kAndS[1];
        
        if (c_i <= k_i) {      // then we found the first empty buffer
            break;
        }
        
        if (PROFILE_ADAPTIVE_PERF) {        // if profile adaptive performance is enabled
            heap_t::iterator leaf_1 = I_NULL;
            heap_t::iterator leaf_2 = I_NULL;
            
            
            // if the number of elements in the lower part of the link is
            // less than or equal to (k_i * s_i)/2, then we can merge two
            // of the leafs and create an empty leaf, thus enabling us to
            // sweep to it.
            if (r_i <= k_i*s_i/2) {
                pap_ci = getLeafsForPAP(link_i, leaf_1, leaf_2);
                
                if (leaf_2 != I_NULL) {   // no empty buffer was found, do the merging
                    // merge the elements into leaf_2
                    std::queue<monom_t> stream1;
                    while (!isBufferEmpty(leaf_1)) {
                        stream1.push(popMax(leaf_1));
                    }
                    
                    std::queue<monom_t> stream2;
                    while (!isBufferEmpty(leaf_2)) {
                        stream2.push(popMax(leaf_2));
                    }
                                                            
                    while (!stream1.empty() && !stream2.empty()) {
                        if (stream1.front() < stream2.front()) {
                            insertInBuffer(leaf_2, stream1.front());
                            stream1.pop();
                        }
                        else {  // if stream2.top < stream1.top
                            insertInBuffer(leaf_2, stream2.front());
                            stream2.pop();
                        }
                    }

                    
                    // now either stream1 OR stream2 is not empty...empty it
                    
                    while (!stream1.empty()) {
                        insertInBuffer(leaf_2, stream1.front());
                        stream1.pop();
                    }
                    
                    while (!stream2.empty()) {
                        insertInBuffer(leaf_2, stream2.front());
                        stream2.pop();
                    }
                    
                }
                
                break;  // we found a suitable buffer, now sweep
            }
        }
        
        // else, go to next link (if it exists)
        A_i = link_i + LINK_OVERHEAD;
        heap_t::iterator temp = gotoLink(heap.begin(), i+1);
        if (temp == I_NULL) {           // if there are no more link, don't insert
            printf("WARNING: Queue is full. Element cannot be inserted!\n");
			
			printQueueState();
            return;
        }
        else {
            link_i = temp;
        }
        
        i++;
    }
    
    if (PROFILE_ADAPTIVE_PERF && pap_ci != -1) {    // set the c_i of the link to the buffer found above
        long newC = pap_ci;
        pap_ci = getEmptyLeafCounter(link_i);
        setEmptyLeafCounter(link_i, newC);
    }

    sweep(I, i);
    setEmptyLeafCounter(link_i, c_i+1); // increment the emptyLeafCounter of link i by 1

    if (PROFILE_ADAPTIVE_PERF && pap_ci != -1) {    // set the c_i of the link back to what it was
        setEmptyLeafCounter(link_i, pap_ci);
    }

}

monom_t private_poll (heap_t::iterator qBegin) {
	just_counter++;
    heap_t::iterator I = qBegin+1;
    heap_t::iterator A_1 = getLeftChild(I);
    
    // if (isBufferEmpty(I) && isExhausted(A_1))
    //     return 0;
    
	// if queue is empty, return 0;
	if ((*qBegin) == 0) {
		printf("THIS\n");
		return 0;
	}
	
    if (isBufferEmpty(A_1)) {
        fill (A_1);
    }

    
    monom_t I_max = getMax(I);
    monom_t A_1_max = getMax(A_1);
    
    monom_t popped;
    if (GET_DEGREE(I_max) > GET_DEGREE(A_1_max) || isExhausted(A_1))
        popped = popMax(I);              // remove I_max
    else
        popped = popMax(A_1);

	// decrease the size of the queue
	(*qBegin) = (*qBegin) - 1;
    
    return popped;
}

monom_t FunnelHeap::poll () {
    // EFFECTS: if all links are empty, then A_1 is set to exhausted
	return private_poll(heap.begin());    
}


// removes an element from the heap, but does not decrement the size of the heap (used in sweep)
monom_t fakePoll (heap_t::iterator qBegin) {
	monom_t element = private_poll(qBegin);
	(*qBegin) = (*qBegin) + 1;
	
	return element;
}

size_t FunnelHeap::size () {
	return (*(heap.begin()));
}

#pragma mark -
#pragma mark Initilization of Queue (sub methods)



/*
 * setQueueDefaultValues: sets all the default values of links and buffers in their correct
 *                          location in the queue structure
 */
void setQueueDefaultValues (heap_t &queue, long numOfLinks) {
    const int s1 = 8;
    
	// let 'it' point to the start of the queue (i.e. to the queue size element)
	heap_t::iterator it = queue.begin();
	(*it) = 0;			// set the initial queue size to 0
		
    // queue[SCLE_INDEX] = packSCLE(0, s1, 0, 1);      // set the capacity of I
    // queue[HI_INDEX] = packHI(0, I_INDEX);              // set index of I to the max number of monom_t
    // setBufferType(queue, I_TYPE);           // set the type of I

	it++;			// let it point to the start of buffer I
		
	// setup buffer I
	(*(it + SCLE_INDEX)) = packSCLE(0, s1, 0, 1);		// set the capacity of I
	(*(it + HI_INDEX)) = packHI(0, I_INDEX);			// set the index of I to max(monom_t)
	setBufferType(it, I_TYPE);						// set the type of the I buffer
    

    // monom_t* parent = queue;         // set I as the parent
    // monom_t* currentLink = gotoLink(queue, 1);// queue + (s1 + BUFFER_OVERHEAD);     // go to first link
	
	heap_t::iterator parent = it;		// set I as the parent
	heap_t::iterator currentLink = gotoLink(queue.begin(), 1);		// go to the first link

	
    
    // // set children of I as A_1, and parent as NULL (0)
    // long I_Child = (currentLink - parent) + LINK_OVERHEAD;
    // 
    // setLeftChild(parent, I_Child);
    // setRightChild(parent, I_Child);
    // setParent(parent, 0);

	// set children of I as A_1, and parent of I as NULL (0)
	long I_CHILD = (long) ((currentLink - parent) + LINK_OVERHEAD);
	
	setLeftChild(parent, I_CHILD);
	setRightChild(parent, I_CHILD);
	setParent(parent, 0);
    
    // // set parent of A_1 as I
    //     monom_t* A_1 = getLeftChild(parent);
    //     setParent(A_1, I_Child);

	// set parent of A_1 as I
	heap_t::iterator A_1 = getLeftChild(parent);
	setParent(A_1, I_CHILD);
	
	

    // loop on the links and set up the default values (of links and buffers)
    for (int i = 1; i <= numOfLinks; i++) {
		// get the capacity of the i'th link
		unsigned long long linkCapacity = computeLinkCapacity(i);
 
        // currentLink[LINK_CAP] = linkCapacity;                          // set link capacity
        // currentLink[TI_INDEX] = packTI(numOfLinks, i);                             // set link index
        // currentLink[EMPTY_LEAF_COUNTER_INDEX] = 1;      // set counter to one
        // currentLink[NUMBER_OF_ELEMENTS_INDEX] = 0;      // set number of elements in link to 0
        
		(*(currentLink + LINK_CAP)) = linkCapacity;				// set the link's capacity
		(*(currentLink + TI_INDEX)) = packTI(numOfLinks, i);	// set the link's index
		(*(currentLink + EMPTY_LEAF_COUNTER_INDEX)) = 1;		// set counter to one
		(*(currentLink + NUMBER_OF_ELEMENTS_INDEX)) = 0;		// set number of elements in link to 0
		
		
        // get the K and S parameters of the current link
        
        long kAndS [2];
        getKAndS(kAndS, i);
        
        long k_i = kAndS[0];
        long s_i = kAndS[1];
        
        long bufferIndex = 0;
        
		// // set capacities for A_i and B_i
		//         monom_t* currentBuffer = currentLink + LINK_OVERHEAD;        // currentBuffer = A_i
		// 
		//         currentBuffer[SCLE_INDEX] = packSCLE(0, pow(k_i,3), 0, 1);      // set capacity of A_i buffer
		//         currentBuffer[HI_INDEX] = packHI(0, bufferIndex++);               // set index of A_i buffer
		//         setBufferType(currentBuffer, A_TYPE);                       // set the type of A
		//         
		//         monom_t* A_i = currentBuffer;            // save A_i
        
		// set capacities for A_i and B_i
		heap_t::iterator currentBuffer = currentLink + LINK_OVERHEAD;			// currentBuffer = A_i
		
		// setup A_i parameters
		(*(currentBuffer + SCLE_INDEX)) = packSCLE(0, (long) pow((double) k_i, 3.0), 0, 1);		// set capacity of A_i buffer
		(*(currentBuffer + HI_INDEX)) = packHI(0, bufferIndex++);				// set index of A_i buffer 
		setBufferType(currentBuffer, A_TYPE);
		
		heap_t::iterator A_i = currentBuffer;		// save A_i reference

        currentBuffer = currentBuffer + (getBufferCapacity(currentBuffer) + BUFFER_OVERHEAD);  // currentBuffer = B_i
        
        
        // set left child of A_i as B_i, and rightChild as A_i+1 (if it exists), and parent of A_i+1 as A_i
        parent = A_i;
        long left = currentBuffer - parent;
        setLeftChild(A_i, left);
        
        long right = 0;
        if (i < numOfLinks) {         // if A_i+1 exists
            right = linkCapacity + LINK_OVERHEAD;
            setParent(A_i + right, right);          // set parent of A_i+1
        }
        
        setRightChild(A_i, right);              // set right child of A_i
        
        parent = A_i;
        
        // set parent of B_i as A_i
        setParent(currentBuffer, left);
        
		// // setup B_i parameters
		//         currentBuffer[SCLE_INDEX] = packSCLE(0, pow(k_i,3), 0, 1);      // set capacity of B_i buffer
		//         currentBuffer[HI_INDEX] = packHI(0, bufferIndex++);               // set index of B_i buffer
		//         setBufferType(currentBuffer, B_TYPE);                           // set the type of B

        // setup B_i parameters
		(*(currentBuffer + SCLE_INDEX)) = packSCLE(0, (long) pow((double) k_i, 3.0), 0, 1);		// set capacity of B_i buffer
		(*(currentBuffer + HI_INDEX)) = packHI(0, bufferIndex++);				// set index of B_i buffer 
		setBufferType(currentBuffer, B_TYPE);									// set the type of B_i
        
        
        
        //monom_t* B_i = currentBuffer;        // save B_i

		heap_t::iterator B_i = currentBuffer;		// save B_i
        
        currentBuffer = currentBuffer + (getBufferCapacity(currentBuffer) + BUFFER_OVERHEAD);  // currentBuffer = K-merger
        
        // STATE: currentBuffer is at the start of the first buffer in K_i merger
        
        // set K_i capacities (for each buffer in K_i)
        updateKCapacity(k_i, currentBuffer, B_i);
        
        // STATE: currentBuffer is at the start of S_i1
        
        
        long height = (long) log2(k_i);
        parent = B_i;
        
        for (int j = 1; j < height; j++) {           // go to leftmost child...ie the one that is parent to S_i1
            parent = getLeftChild(parent);    
        }
        
		stack<char> state;
        
        for (int j = 0; j < height; j++) {     // set up the state for finding all leaves
            state.push('l');
        }
        
        // set capacities for the leafs: S_i1 to S_iki
        
        long tester = currentBuffer - B_i;
        
        for (int j = 0; j < k_i; j+=2) {
            if (j == 16) {
                tester = currentBuffer - B_i;
            }
            setLeftChild(parent, currentBuffer-parent);         // set left child of parent
            
            // currentBuffer[SCLE_INDEX] = packSCLE(0, s_i, 1, 1);
            // currentBuffer[HI_INDEX] = packHI(0, bufferIndex++);

			// setup the parent's left child
			(*(currentBuffer + SCLE_INDEX)) = packSCLE(0, s_i, 1, 1);
			(*(currentBuffer + HI_INDEX)) = packHI(0, bufferIndex++);
			
            setBufferType(currentBuffer, LEAF_TYPE);        // set the type to leaf
            
            setLeftRightParent(currentBuffer, 0, 0, currentBuffer-parent);
            
            currentBuffer = currentBuffer + (getBufferCapacity(currentBuffer) + BUFFER_OVERHEAD);      // go to right child
            
            setRightChild(parent, currentBuffer - parent);          // set right child of parent
            
            // currentBuffer[SCLE_INDEX] = packSCLE(0, s_i, 1, 1);
            // currentBuffer[HI_INDEX] = packHI(0, bufferIndex++);

			(*(currentBuffer + SCLE_INDEX)) = packSCLE(0, s_i, 1, 1);
			(*(currentBuffer + HI_INDEX)) = packHI(0, bufferIndex++);
			
            setBufferType(currentBuffer, LEAF_TYPE);                // set the type to leaf
            
            setLeftRightParent(currentBuffer, 0, 0, currentBuffer-parent);
            
            currentBuffer = currentBuffer + (getBufferCapacity(currentBuffer) + BUFFER_OVERHEAD);
            
            parent = nextNodeAtHeight(parent, height, state);
            if (parent == I_NULL) {
                if (j+2 != k_i) {
                    // internal error
                    printf("INTERNAL ERROR: Did not access all parents of S_i1...S_iki\n");
                }
                
                break;
            }
        }
                
        // go to next link
        currentLink = currentLink + (*currentLink) + LINK_OVERHEAD;     // currentLink = start of link(i+1)
    }
}


/*
 * updateKCapacity: updates the capacities of the buffers in the k-merger in memory, guided by
 *                  the recursive layout.
 *
 * Parameters:
 *      - k: the number of input streams that the K-merger can merge
 *      - mem: a pointer to the memory location where the subtree will be placed
 *      - parent_mem: a pointer to the memory location where the output buffer of the subtree
 *                      pointed to by mem is located
 *
 * Precondition: 
 *      - k is a multiple of 2
 *      
 */
long updateKCapacity (long k, heap_t::iterator &mem, heap_t::iterator parent_mem) {
    
    
    
    if (k == 1) {
        // internal error
        printf("ERROR: K = 1\n");
        return 0;
    }
    
    if (k == 2) {           // no mid buffers to allocate
        return 0;
    }
    
    if (k == 4) {       // allocate memory for the two mid buffers
        int bufferCapacity = (int) ceil(pow(k, 1.5));
        
        monom_t leftChild = 0;
        monom_t rightChild = 0;
        
        for (int i = 0; i < 2; i++) {
            (*mem) = packSCLE(0, bufferCapacity, 0, 1);
            setParent(mem, mem - parent_mem);
            
            if (i == 0) {
                leftChild = mem - parent_mem;
            }
            else {
                rightChild = mem - parent_mem;
            }
            mem = mem + (bufferCapacity + BUFFER_OVERHEAD);         // go to the next buffer
            
            
        }

        setLeftChild(parent_mem, leftChild);
        setRightChild(parent_mem, rightChild);
        if (getBufferType(parent_mem) != B_TYPE) {
            setBufferType(parent_mem, INTERIOR_TYPE);
        }

        // STATE: mem now points to the empty location right after the space for the above two buffers
        
        return (2 * (bufferCapacity + BUFFER_OVERHEAD));
    }
    
    int height = (int) log2(k);
    
    int top_i = (int) ceil(height/2.0);
    int bottom_i = height - top_i;
    
    int top_k = (int) pow(2.0, (double) top_i);      // get the k of the top subtree
    int bottom_k = (int) pow(2.0, (double) bottom_i);    // get the k of the bottom subtrees
    
    int midBufferSize = (int) ceil(pow(k, 1.5));
    
    
    
    long topTreeSize = updateKCapacity(top_k, mem, parent_mem);        // recursively layout the top subtree
    
    // STATE: mem now points to the first empty location after the top subtree
    
    parent_mem = mem;
    parent_mem = parent_mem - topTreeSize;      // go back to top (ie first buffer in level that has 2 buffers)
    
    for (int i = 2; i < top_i; i++) {           // go to leftmost child...ie the one that is parent to first mid buffer
        parent_mem = getLeftChild(parent_mem);     
    }
    
	stack<char> state;
    
    for (int i = 0; i < top_i; i++) {     // set up the state for finding all leaves
        state.push('l');
    }
    
    
    heap_t::iterator midBuffers [top_k];
    for (int i = 0; i < top_k; i++) {       // layout the middle buffers
        (*mem) = packSCLE(0, midBufferSize, 0, 1);
        midBuffers[i] = mem;        // store pointer to mid buffer
        long toSkip = mem - parent_mem;
        
        // set parent
        setLeftRightParent(mem, 0, 0, toSkip); 
        
        mem = mem + (midBufferSize + BUFFER_OVERHEAD);
        
        
        // set left/right child of parent
        if (i % 2 == 0) {
            // set left child
            //monom_t lrp = parent_mem[LR_INDEX];
            
            // parent_mem[LR_INDEX] = lrp | (toSkip << L_SHIFT);
            
            setLeftChild(parent_mem, toSkip);
            
            
            
        }
        else if (i % 2 == 1) {
            // set right child and go to next parent
            
            //monom_t lrp = parent_mem[LR_INDEX];
            
            //parent_mem[LR_INDEX] = lrp | (toSkip << R_SHIFT);
            
            setRightChild(parent_mem, toSkip);
            
            parent_mem = nextNodeAtHeight(parent_mem, top_i, state);
            if (parent_mem == I_NULL) {
                break;
            }
        }
    }
        
    // STATE: mem now points to the first empty location after the middle buffers
    
    long bottomTreeSize = -1;
    for (int i = 0; i < top_k; i++) {
        parent_mem = midBuffers[i];
        bottomTreeSize = updateKCapacity(bottom_k, mem, parent_mem);     // recursively layout each bottom subtree
    }
    
    //return topTreeSize + (top_k * midBufferSize) + (top_k * bottomTreeSize);
    return topTreeSize + (top_k * (midBufferSize + BUFFER_OVERHEAD)) + (top_k * bottomTreeSize);
}



#pragma mark -
#pragma mark Traversing Queue

/*
 * gotoLink: returns a pointer to the head of the i'th link
 *
 * Precondition:
 *      - 'queue' points to the beginning of the queue (i.e. to the size of the queue)
 */
heap_t::iterator gotoLink (heap_t::iterator queue, int i) {
    if (i < 1) 
        return I_NULL;

	heap_t::iterator I = queue + 1;		// I points to the I-buffer
    
    // skip the I buffer
    long I_capacity = getBufferCapacity(I);
    
    heap_t::iterator link = I;
    link = link + I_capacity + BUFFER_OVERHEAD + LINK_OVERHEAD;       // go to A_1
    
    for (int j = 2; j <= i; j++) {  
        link = getRightChild(link);     // goto A_j 
        
        if (link == I_NULL)       // if A_j does not exist
            return link;
        
            
    }
    
    link = link - LINK_OVERHEAD;
    return link;
    
}




#pragma mark -
#pragma mark Computing Capacities

/* 
 * getSAndK: computes s_i and k_i of link i, and puts the value of k_i at 
 *           index 0, and s_i at index 1 in the values array
 */
inline void getKAndS (long values [], int i) {
    
    /************************ Definition of k_i and s_i *************************
     *                                                                          *
     * (k_1, s_1) = (2,8)                                                       *
     * s_{i+1} = s_i (k_i + 1)                                                  *
     * k_{i+1} = 2^{log((s_{i+1})^{1/3})}   (where log(x^{1/3}) is rounded up)  *
     *                                                                          *
     ****************************************************************************/
    
    long k_i = 2, s_i = 8;
    
    for (int count = 2; count <= i; count++) {
        s_i = s_i * (k_i + 1);
        
        long temp = ceil(log2(pow(s_i, 1/3.0)));
        k_i = (long) pow(2.0, (double) temp);
    }
    
    values[0] = k_i;
    values[1] = s_i;
}

/*
 * computeLinkSize: computes the size of the i'th link (the number of monom_t elements)
 *                  NOTE: The link capacity is the number of elements that can fit 
 *                  from A_i to s_iki
 */
unsigned long long computeLinkCapacity(int i) {
    
    /********************* Definition of Link i *************************
     *                                                                  *
     * Link capacity and index                                          *
     * c_i                                                              *
     * k_i buffers, each of size s_i                                    *
     * K_i merger (funnel) that mergers the buffers                     *
     * an output buffer B_i of size (k_i)^3                             *
     * v_i merger, that mergers b_i with A_{i+1}                        *
     * A_i buffer, the output of the v_i merger, of size (k_i)^3        *
     *                                                                  *
     * Structure of link i in memory:                                   *
     *      _________________________________________________________   *
     *     | Cap | Index | c_i | A_i | B_i |  K_i  | S_i1 |...| S_ik |  *
     *      ---------------------------------------------------------   *
     *                                                                  *
     ********************************************************************/
    
   
    long kAndS [2];
    getKAndS(kAndS, i);
    
    unsigned long long k_i = kAndS[0];
    unsigned long long s_i = kAndS[1];
	
    unsigned long long totalSize = k_i * s_i + (k_i * BUFFER_OVERHEAD);     // num of elem in each buffer plus buffer overhead for each buffer
    totalSize += 2 * ((k_i*k_i*k_i) + BUFFER_OVERHEAD) ;   // num of elements in A_i and B_i plus the packing and index
    
    totalSize += kSize(k_i) + (k_i - 2) * BUFFER_OVERHEAD;      // compute size of k-merger
    
    
    return totalSize;      
}




#pragma mark -
#pragma mark Setting/Accessing Parent and Children


/*
 * setLeftChild: sets the left child of a buffer
 *
 * Precondition: 
 *      - the pointer must point to the beginning of the buffer, ie to sce
 */
inline void setLeftChild(heap_t::iterator buffer, monom_t left) {    
    (*(buffer + LEFT_CHILD_INDEX)) = left;
}

/*
 * setRightChild: sets the right child of a buffer
 *
 * Precondition: 
 *      - the pointer must point to the beginning of the buffer, ie to sce
 */
inline void setRightChild (heap_t::iterator buffer, monom_t right) {
    (*(buffer + RIGHT_CHILD_INDEX)) = right;
}


/*
 * setParent: sets the parent of a buffer
 *
 * Precondition:
 *      - the pointer must point to the beginning of the buffer, ie to sce
 */
inline void setParent (heap_t::iterator buffer, monom_t parent) {
    (*(buffer + PARENT_INDEX)) = parent;
}


/*
 * setLeftRightParent: sets the left child, right child, and parent of the current buffer
 */
inline void setLeftRightParent (heap_t::iterator buffer, monom_t left, monom_t right, monom_t parent) {
    setLeftChild(buffer, left);
    setRightChild(buffer, right);
    setParent(buffer, parent);
}


/*
 * getLeftChild: returns a pointer to the beginning of the left child, and NULL if 
 *               parent is a leaf
 *
 * Precondition: 
 *      - the pointer of the parent must point to beginning of the buffer
 */
inline heap_t::iterator getLeftChild (heap_t::iterator parent) {
    heap_t::iterator left = I_NULL;
    
    monom_t toSkip = (*(parent + LEFT_CHILD_INDEX));
    
    if (toSkip != 0) {
        left = parent + toSkip;
    }
    
    return left;
}


/*
 * getRightChild: returns a pointer to the beginning of the right child, and NULL if 
 *                parent is a leaf
 *
 * Precondition: 
 *      - the pointer of the parent must point to beginning of the buffer
 */
inline heap_t::iterator getRightChild (heap_t::iterator parent) {
    heap_t::iterator right = I_NULL;
    
    monom_t toSkip = (*(parent + RIGHT_CHILD_INDEX));
    
    if (toSkip != 0) {
        right = parent + toSkip;
    }
    
    return right;
}


/*
 * getParent: returns a pointer to the beginning of the parent, and NULL if
 *            child is I
 *
 * Precondition: 
 *      - the pointer of the child must point to beginning of the buffer
 */
inline heap_t::iterator getParent (heap_t::iterator child) {
    heap_t::iterator parent = I_NULL;
    
    monom_t toSkip = (*(child + PARENT_INDEX));
    
    if (toSkip != 0) {
        parent = child - toSkip;
    }
    
    return parent;
}



#pragma mark -
#pragma mark Setting/Accessing Buffer Attributes



/*
 * getBufferSize: returns the size of the buffer, ie the number of elements currently in it
 */
inline long getBufferSize (heap_t::iterator buffer) {
    return UNPACK_SIZE((*(buffer + SCLE_INDEX)));
}

/*
 * getBufferCapacity: returns the capacity of the buffer
 */
inline long getBufferCapacity (heap_t::iterator buffer) {
    return UNPACK_CAPACITY((*(buffer + SCLE_INDEX)));
}

/* 
 * isExhausted: returns true if buffer stream is exhausted, or if buffer is NULL
 */
inline bool isExhausted (heap_t::iterator buffer) {
    if (buffer == I_NULL) {
        return true;
    }
    
    return (UNPACK_EXHAUSTED((*(buffer + SCLE_INDEX))) != 0) ? true : false;
}

/*
 * setBufferSize: sets the size of the buffer
 */
inline void setBufferSize (heap_t::iterator buffer, monom_t size) {
    (*(buffer + SCLE_INDEX)) = ((*(buffer + SCLE_INDEX)) & ZERO_SIZE) | (size << SIZE_SHIFT);
}

/*
 * setBufferCapacity: sets the capacity of the buffer
 */
inline void setBufferCapacity (heap_t::iterator buffer, monom_t capacity) {
    (*(buffer + SCLE_INDEX)) = ((*(buffer + SCLE_INDEX)) & ZERO_CAP) | (capacity << CAP_SHIFT);
}

inline void setExhausted (heap_t::iterator buffer, bool isExhausted) {
    if (isExhausted)
        (*(buffer + SCLE_INDEX)) = (*(buffer + SCLE_INDEX)) | EXH_MASK;
    else
        (*(buffer + SCLE_INDEX)) = (*(buffer + SCLE_INDEX)) & ZERO_EXH;       // set to false
}

/*
 * isBufferEmpty: returns true if buffer is empty
 */
inline bool isBufferEmpty(heap_t::iterator buffer) {
    if (getBufferSize(buffer) == 0) 
        return true;
    
    // else
    return false;
}

/*
 * isBufferFull: returns true if buffer is full
 */
inline bool isBufferFull(heap_t::iterator buffer) {
    if (getBufferSize(buffer) == getBufferCapacity(buffer)) 
        return true;
    
    // else
    return false;
}

/*
 * isLeaf: returns true if the current buffer is a leaf in the binary tree
 */
inline bool isLeaf (heap_t::iterator buffer) {
    if (buffer == I_NULL)
        return false;
    
	return (((int)getBufferType(buffer)) == LEAF_TYPE);
    // long value = UNPACK_IS_LEAF((*(buffer + SCLE_INDEX)));
    //     if (value == 1) {
    //         return true;
    //     }
    //     
    //     return false;
}

/*
 * getMax: returns the maximum element in the buffer. Returns 0
 *         if the buffer is empty
 */
inline monom_t getMax (heap_t::iterator buffer) {
    if (getBufferSize(buffer) == 0) {
        return 0;
    }

	// return: buffer[BUFFER_OVERHEAD + location of head]
    return (*(buffer + (BUFFER_OVERHEAD + UNPACK_HEAD((*(buffer + HT_INDEX))))));
}

/*
 * getEmptyLeafCounter: returns the counter that indexes the first empty leaf in that link
 */
inline monom_t getEmptyLeafCounter (heap_t::iterator link_i) {
    return (*(link_i + EMPTY_LEAF_COUNTER_INDEX));
}
    
/*
 * setEmptyLeafCounter: sets the value of the counter that indexes the first empty leaf in that link
 */
inline void setEmptyLeafCounter (heap_t::iterator link_i, monom_t c_i) {
    (*(link_i + EMPTY_LEAF_COUNTER_INDEX)) = c_i;
}

/*
 *  getNumberOfElementsInLower: returns the number of elements in the lower part of the link
 *                                  (ie, from B_i till leafs)
 */
inline monom_t getNumberOfElementsInLower (heap_t::iterator link_i) {
    return (*(link_i + NUMBER_OF_ELEMENTS_INDEX));
}

/*
 *  setNumberOfElementsInLower: sets the number of elements in the lower part of the link
 */
inline void setNumberOfElementsInLower (heap_t::iterator link_i, monom_t number) {
    (*(link_i + NUMBER_OF_ELEMENTS_INDEX)) = number;
}

/*
 *  incrementNumberOfElementsInLowerBy: increments the number of elements in the lower part of 
 *                                          the link by 'increment'
 *
 *  @param increment: the number by which to increment
 */
inline void incrementNumberOfElementsInLowerBy(heap_t::iterator link_i, monom_t increment) {
    (*(link_i + NUMBER_OF_ELEMENTS_INDEX)) = (*(link_i + NUMBER_OF_ELEMENTS_INDEX)) + increment;
}

/*
 *  decrementNumberOfElementsInLowerBy: increments the number of elements in the lower part of 
 *                                          the link by 'increment'
 *
 *  @param decrement: the number by which to decrement
 */
inline void decrementNumberOfElementsInLowerBy(heap_t::iterator link_i, monom_t decrement) {
    (*(link_i + NUMBER_OF_ELEMENTS_INDEX)) = (*(link_i + NUMBER_OF_ELEMENTS_INDEX)) - decrement;
}

#pragma mark -
#pragma mark Buffer Elements Manipluation

/*********************** BUFFER ELEMENTS MANIPULATION *************************\
 *
 *  IMPORTANT:
 *
 *  1) The elements in a buffer are stored in non-increasing order, from 
 *      left to right (where the leftmost element is at index 0, with respect
 *      to the first position of the elements' array)
 *
 *  2) The array of elements simulates a circular buffer, ie the head and tail are
 *      represented by indeces. This saves on time when we need to insert elements 
 *      into the sorted array. Insert now costs O(1). Note that this assumes that
 *      an element inserted into that buffer is less than or equal to the minimum
 *      element in that buffer.
 *
 *  3) The only exception to (2) is the buffer I. This buffer requires us to find the
 *      position of the new element in the array, since we have no constraints on the
 *      value of that element.
 *
 *  4) To correctly understand the functions nextElement and previousElement, the 
 *      elements should be visuallized as a sequence of consecutive, sorted elements,
 *      decreasing from left to right, disregarding the fact that the buffer is a circular 
 *      buffer. Therefore, calling nextElement would be used to access the next 'smaller'
 *      element, whereas calling previousElement would be used to access the next 'larger'
 *      element.
 *
 */

/*
 *  insertAtPosition: inserts an element at a certain position in the buffer
 *
 *  @param buffer: a pointer to the buffer
 *  @param element: the element to be inserted
 *  @param position: the position in which to insert the element
 *
 *  @precondition: 0 <= position < buffer.size()
 */
inline void insertAtPosition (heap_t::iterator buffer, monom_t element, long position) {
    (*(buffer + (BUFFER_OVERHEAD + position))) = element;
    setBufferSize(buffer, getBufferSize(buffer)+1);
}

/*
 *  getElementAtPosition: returns the element at a certain position
 *
 *  @param buffer: pointer to the buffer
 *  @param position: the position of the element to be retrieved
 *
 *  @precondition: 0 <= position < buffer.size()
 */
inline monom_t getElementAtPosition (heap_t::iterator buffer, monom_t position) {
    return (*(buffer + (BUFFER_OVERHEAD + position)));
}

/*
 *  insertInBuffer: inserts an element in the buffer (different than I) while mainting a
 *                  sorted order
 *
 *  @param buffer: pointer to the buffer
 *  @param element: the element to be inserted
 *
 *  @precondition: element should be less than all elements in the buffer
 */
void insertInBuffer (heap_t::iterator buffer, monom_t element) {
    if (isBufferFull(buffer)) {
        return;
    }
    
    long size = getBufferSize(buffer);
    long capacity = getBufferCapacity(buffer);
    //monom_t* array = buffer + BUFFER_OVERHEAD;       // point to the elements
    
    long head = UNPACK_HEAD((*(buffer + HT_INDEX)));
    long tail = UNPACK_TAIL((*(buffer + HT_INDEX)));
    
    if (size == 0) {
        head = tail = 0;
        insertAtPosition(buffer, element, head);
        (*(buffer + HT_INDEX)) = packHT(head, tail);
        setExhausted(buffer, false);
        return;
    }
    
    
    newTailIndex(tail, size, capacity);
    
    insertAtPosition(buffer, element, tail);
    (*(buffer + HT_INDEX)) = packHT(head, tail);
}

/*
 *  insertInI: inserts the element in buffer I
 *
 *  @param I: pointer to I
 *  @param degree: the degree of the element to be inserted
 *  @param coef: the coefficient of the element to be inserted
 *  @param f_id: the id of the current monomial
 */
void insertInI (heap_t::iterator I, monom_t degree, monom_t coef, monom_t f_id) {
    
    
    // If buffer I is full, then return. NOTE: If I is full at this point, then
    // there must be a logical error in the Funnel Heap code, since I is always 
    // emptied the moment we insert the last element.
    if (isBufferFull(I)) {
        cout << "Internal Warning: Attempted to insert in I while I was full." << endl;
        return;
    }
    
    monom_t element = createMonomial(degree, coef, f_id);
    
    long size = getBufferSize(I);
    long capacity = getBufferCapacity(I);
    heap_t::iterator array = I + BUFFER_OVERHEAD;
    
    long head = UNPACK_HEAD((*(I + HT_INDEX)));
    long tail = UNPACK_TAIL((*(I + HT_INDEX)));
    
    if (size == 0) {
        head = tail = 0;
        insertAtPosition(I, element, head);
        (*(I + HT_INDEX)) = packHT(head, tail);
        return;
    }
    
    long pos = tail;
    long j = 0;
    
    while (j < size) {              // find position of new element
        
        // keep searching for a position in which to place
        // the new element in order to main the sorted order defined
        // in the buffer I
        if (degree > GET_DEGREE((*(array + pos)))) {
            previousElement(pos, capacity);
            j++;
        }
        else
            break;
    }
    
    if (j == size) {            // element is max (at head)
        newHeadIndex(head, size, capacity);      // make position for head
        insertAtPosition(I, element, head);
        (*(I + HT_INDEX)) = packHT(head, tail);
        return;
    }
    
    
    // else shift elements from pos...head by one step and insert element at pos
    newHeadIndex(head, size, capacity);      // make room for new head
    long index = head;
    long current = index;
    
    while (index != pos) {
        nextElement(index, capacity);     // to to previous position
        (*(array + current)) = (*(array + index));
        
        current = index;
    }
    
    insertAtPosition(I, element, pos);
    (*(I + HT_INDEX)) = packHT(head, tail);
}


/*
 * The below 4 methods are used to get the next/previous index of the
 * head and tail of the elements array
 */

/*
 * nextElement: returns the next element index (ie index of element with smaller value)
 */
inline void nextElement (long &pos, long capacity) {
    if (pos == capacity - 1) {
        pos = 0;
    }
    else
        pos++;
}

/*
 * previousElement: returns the previous element index (ie index of element with larger value)
 */
inline void previousElement (long &pos, long capacity) {
    if (pos == 0) {
        pos = capacity-1;
    }
    else 
        pos--;
}

/*
 *  newHeadIndex: changes the value of 'head' to point to the next empty space after
 *                  the first element in the buffer (or the head)
 */
inline void newHeadIndex (long &head, long size, long capacity) {
    if (size == capacity) {
        printf("Warning: No more room in buffer!\n");
        return;
    }
    
    
    if (head == 0) {
        head = capacity-1;
    }
    else
        head--;
}

/*
 *  newTailIndex: changes the value of 'tail' to point to the next empty space after 
 *                  the last element in the buffer
 */
inline void newTailIndex (long &tail, long size, long capacity) {
    if (size == capacity) {
        printf("Warning: No more room in buffer!\n");
        return;
    }
    
    if (tail == capacity-1) {
        tail = 0;
    }
    else
        tail++;
}





#pragma mark -
#pragma mark Sweep, Fill...



/*
 *  sweep: used to empty buffer I and make room for new elements to be inserted
 *
 *  @param q: pointer to the q 
 *  @param linkIndex: the index of the link where the values in I will be "sweeped" into
 */
void sweep (heap_t::iterator I, int linkIndex) {
    /* Sweep Algorithm *\
     *
     * - Effect: + Move the content of links 1,...,i-1 to the 
     *             buffer S_ici, where i = linkIndex.
     *           + This means that all links less than i will 
     *             be empty.
     *
     * - Code Sketch:
     *      1) Traverse path 'p' from A_1 to S_ici, and:
     *          a) Record how many elements each buffer on this
     *             path currently contains.
     *          b) From sorted stream_1 of the elements in the 
     *             part of path 'p' from A_i to S_ici
     *      2) From sorted stream_2 of all elements in links less
     *         than i and in buffer I, by marking A_i as exhausted
     *         and callining poll() repeatedly.
     *      3) Merge the two streams, and traverse 'p' again while
     *         inserting the front elements of the new stream in
     *         the buffers on 'p' in such a way that these buffers
     *         contain the same numbers of elements as before the
     *         insertion, and then insert the remaining elements
     *         in S_ici.
     *      4) Reset the 'c' counter for all the links less than i
     *         back to 1, and increment c_i by 1.
     *  
     *
     */

    // form stream1
    // queue<monom_t> stream1;	//HERE
	stream_t stream1;
	
	heap_t::iterator q = I-1;
	
    heap_t::iterator link_i = gotoLink(q, linkIndex);
    monom_t c_i = (*(link_i + EMPTY_LEAF_COUNTER_INDEX));                     // get c_i
    heap_t::iterator A_sweep = link_i + LINK_OVERHEAD;            // A_sweep is the A buffer of the link being sweeped to
    

    // store all sizes of path from A_sweep to S_sweep_ci
	queue<long> partOfPSizes;
    
    formStream1(A_sweep, c_i, linkIndex, stream1, partOfPSizes);
   

	// set A_sweep as exhausted
	setExhausted(A_sweep, true);
	
    monom_t accumRemove = 0;     // amount currently removed from queue
    
    // number of elements in stream 1
    if (ASSERTIONS) {
        queue<long> copy_sizes (partOfPSizes);
    
        monom_t sizes_stored = 0;
        for (size_t i = 0; i < partOfPSizes.size(); i++) {
            sizes_stored += copy_sizes.front();
            copy_sizes.pop();
        }
        
        assert(stream1.size() == sizes_stored);
        
        accumRemove = sizes_stored;
        
    }
    // STATE: stream1 contains all elements in path  A_sweep to S_sweep_ci            
    // NOTE THAT A_sweep SHOULD BE MARKED AS EXHAUSTED BY NOW...
    
    
    // store sizes of all A_i where i < linkIndex
    long A_sizes [linkIndex-1];
    heap_t::iterator A_i = getLeftChild(I);              // A_i = A_1
    
    for (int i = 0; i < linkIndex-1; i++) {     // get the sizes of A_i where i < linkIndex
        A_sizes[i] = getBufferSize(A_i);
        A_i = getRightChild(A_i);
        
    }
    
    
    setExhausted(A_sweep, true);
    //queue<monom_t> stream2;		// HERE
	stream_t stream2;
    
    // for debugging
    monom_t r_i = 0;
    heap_t::iterator temp_link;
    for (int i = 0; i < linkIndex-1; i++) {
        temp_link = gotoLink(q, i+1);
        r_i += getNumberOfElementsInLower(temp_link);
    }

    // form stream 2
    formStream2(q, linkIndex, stream2);   
    
    if (ASSERTIONS) {
        monom_t size_before = r_i + 8;
        for (size_t i = 0; i < linkIndex-1; i++) {
            size_before += A_sizes[i];
        }
                
        assert(stream2.size() == size_before);
        
        accumRemove += size_before;
        
//        if (temp_assertions){
//            assertThereAreXElementsRemaining(myQueue, 6, eInQ-accumRemove);
//        }
    }
    

//    if (ASSERTIONS) {
//        assert(stream.size() == accumRemove);
//    }

    // insert the elements in the stream back into the heap
    insertStreamToPath(getLeftChild(I), stream1, stream2, A_sizes, partOfPSizes, c_i, linkIndex);
        
    // reset c_i and r_i (for i < linkindex) to 1
    heap_t::iterator resetter = I_NULL;
    for (int i = 0; i < linkIndex-1; i++) {
        resetter = gotoLink(q, i+1);
        (*(resetter + EMPTY_LEAF_COUNTER_INDEX)) = 1;     
      //  resetter[NUMBER_OF_ELEMENTS_INDEX] = 0;
    }
    
    
    // increment c_i (for i = linkIndex) by 1
    (*(link_i + EMPTY_LEAF_COUNTER_INDEX)) += 1;
    
}

/*
 *  mergeStreams: merges two streams into one, maintaining their sorted order
 *
 *  @param stream1: the first stream
 *  @param stream2: the second stream
 *  @param output:  the output stream (merging of the two streams)
 */
void mergeStreams (stream_t& stream1, stream_t& stream2, stream_t& output) {
    // while both streams are not empty...
    while (!stream1.empty() && !stream2.empty()) {
        
        monom_t e1 = stream1.front();
        monom_t e2 = stream2.front();
        
        if (e1 > e2) {
            output.push(e1);
            stream1.pop();
        }
        else {
            output.push(e2);
            stream2.pop();
        }
    }
    
    // extract remaining elements from the non-empty stream
    stream_t * non_empty_stream;
    if (!stream1.empty()) {
        non_empty_stream = &stream1;
    }
    else {
        non_empty_stream = &stream2;
    }
    
    while (!non_empty_stream->empty()) {
        output.push(non_empty_stream->front());
        non_empty_stream->pop();
    }
}


/*
 *  pop: extracts the maximum element from the two streams.
 *
 *  @param stream1: the first stream of elements
 *  @param stream2: the second stream of elements
 */
inline monom_t pop(stream_t& stream1, stream_t& stream2) {
    /*
     * General Code Sketch:
     *
     *  1) Extract the element that has the highest degree in both streams
     *  2) Return it
     */
    
    if (stream1.empty() && stream2.empty())
        return 0;
    
    // Find the max element in both streams
    monom_t maxElement = 0;
    if (!stream1.empty())
        maxElement = stream1.front();
    
    if (!stream2.empty()) {
        if (GET_DEGREE(stream2.front()) >= GET_DEGREE(maxElement)) {
            // the max is from stream2
            maxElement = stream2.front();
            stream2.pop();
        }
        else {
            // the max is from stream1
            stream1.pop();
        }
    }
    else {      // the max was from stream1 (since stream2 is empty)
        stream1.pop();
    }

    return maxElement;      
}

inline bool empty(stream_t& stream1, stream_t& stream2) {
    return stream1.empty() && stream2.empty();
}
/*
 *  formStream1: forms the first stream by taking all values from A_i up to S_ici and placing them
 *                  in sorted order in a stream
 *
 *  @param A_i: pointer to buffer A in link i
 *  @param c_i: the index of the first empty leaf (in link i) after which all leafs are empty
 *  @param linkIndex: the index of the link
 *  @param stream1: the stream where the extracted elements will be placed
 *  @param pathSizes: used to store the sizes of the buffers from A_i to S_ici (for later use)
 */
void formStream1 (heap_t::iterator A_i, monom_t c_i, long linkIndex, stream_t& stream1, queue<long> &pathSizes) {
    // form stream 1
    heap_t::iterator currentBuffer = A_i;
    heap_t::iterator link_i = A_i - LINK_OVERHEAD;
    
    long A_i_size = getBufferSize(currentBuffer);
    pathSizes.push(A_i_size);
        
    for (int i = 0; i < A_i_size; i++) {
        stream1.push(getMax(A_i));
        popMax(A_i);
    }

    
    currentBuffer = getLeftChild(currentBuffer);      // go to B_i
    long B_i_size = getBufferSize(currentBuffer);
    pathSizes.push(B_i_size);
    
    monom_t amountRemovedFromLower = 0;      // indicates the number of elements removed
    // from the lower part of the link
    
    for (int i = 0; i < B_i_size; i++) {
        stream1.push(getMax(currentBuffer));;
        popMax(currentBuffer);
        amountRemovedFromLower++;
    }
    
    
    
    long height = linkIndex;
    
	// temp solution for k-merger height at links 6 and 7
    if (linkIndex == 6) height++;
    if (linkIndex == 7) height+=2;
    
	stack<char> path_i;
	getPath(c_i, height, path_i);
    
    // extract elements in path and place in stream1...
    for (int i = 0; i < height; i++) {
        char step = path_i.top();
        path_i.pop();
        
        if (step == 'l') {
            currentBuffer = getLeftChild(currentBuffer);
        }
        else {
            currentBuffer = getRightChild(currentBuffer);
        }
        
        // empty this buffer
        long temp_size = getBufferSize(currentBuffer);
        pathSizes.push(temp_size);
        
        for (int j = 0; j < temp_size; j++) {
            stream1.push(getMax(currentBuffer));
            popMax(currentBuffer);
            amountRemovedFromLower++;
        }
    }
    
    if (ASSERTIONS) {
        assert(amountRemovedFromLower <= getNumberOfElementsInLower(link_i));
    }
    
    decrementNumberOfElementsInLowerBy(link_i, amountRemovedFromLower);      // reset the number of elements in lower part of the link
    
    
}

/*
 *  formStream2: forms the second stream by taking all values in the queue (including I) that are
 *                  in links 1 to j, where j < linkIndex
 *
 *  @param q: pointer to the start of the queue
 *  @param linkIndex: the index of the link
 *  @param stream2: the stream where the extracted elements will be placed
 */
void formStream2 (heap_t::iterator q, long linkIndex, stream_t& stream2) {
    heap_t::iterator I = q+1;
    heap_t::iterator A_1 = getLeftChild(I);
    
	 
	heap_t::iterator B_1 = getLeftChild(A_1);
	heap_t::iterator Leaf_1 = getLeftChild(B_1);
	heap_t::iterator Leaf_2 = getRightChild(B_1);
	if (check_for_prob) {
		// printf("A_1\n"); printBufferElements(A_1); printBuffer(A_1); printf("\n\n");
		// printf("B_1\n"); printBufferElements(B_1); printBuffer(B_1); printf("\n\n");
		// printf("Leaf_1\n"); printBufferElements(Leaf_1); printBuffer(Leaf_1); printf("\n\n");
		// printf("Leaf_2\n"); printBufferElements(Leaf_2); printBuffer(Leaf_2); printf("\n\n");
		
		printQueueElements(q);
	}

	
    // 'empty' the queue
    while (!isBufferEmpty(A_1) || !isBufferEmpty(I)) {
//        stream2.push(poll(q));
		stream2.push(fakePoll(q));
        
		if (check_for_prob) {
			// printf("A_1\n"); printBufferElements(A_1); printBuffer(A_1); printf("\n\n");
			// printf("B_1\n"); printBufferElements(B_1); printBuffer(B_1); printf("\n\n");
			// printf("Leaf_1\n"); printBufferElements(Leaf_1); printBuffer(Leaf_1); printf("\n\n");
			// printf("Leaf_2\n"); printBufferElements(Leaf_2); printBuffer(Leaf_2); printf("\n\n");
			
			printQueueElements(q);
		}
		
		if (((*q) == 0)) {
			assert(isExhausted(A_1));
		}
        if (isBufferEmpty(A_1)) {
            fill(A_1);
        }
    }
    
    assert(isExhausted(A_1));
    //setExhausted(A_1, true);
    
}

/*
 *  insertStreamToPath: inserts the stream back into the queue along the path starting from A_1, 
 *                      and leading to leaf c_i in link 'linkIndex'
 *
 *  @param A_1: pointer to buffer A in link i
 *  @param stream: the stream of elements to be inserted into the queue
 *  @param A_sizes: an array of the previous sizes of buffers A_1...A_j (where j < linkIndex)
 *  @param partOfPSizes: a queue of the previous sizes of the buffers A_i to S_ici
 *  @param ci: the index of the first empty leaf (in link i) after which all leafs are empty
 *  @param linkIndex: the index of the i'th link
 */
void insertStreamToPath (heap_t::iterator A_1, stream_t& stream1, stream_t& stream2, long A_sizes[], queue<long> &partOfPSizes, monom_t c_i, long linkIndex) {
    heap_t::iterator A_i = A_1;      // A_i = A_1
    monom_t amountInsertedIntoLower = 0;     // keep track of amount inserted into the lower part of the link
    
    
    // first fill all the buffers A_i (i < linkIndex)
    
    for (int sizes_index = 0; sizes_index < linkIndex-1; sizes_index++) {
        long size_i =  A_sizes[sizes_index];
        
        // insert size_i elements in buffer from stream
        for (int j = 0; j < size_i; j++) {
            //insertInBuffer(A_i, stream.front());
//            insertInBufferWithoutIncSize(A_i, stream.front());
            
            (*(A_i + (BUFFER_OVERHEAD + j))) = pop(stream1, stream2);   //stream.front();
            
            // if both streams are empty, then we're done
            if (empty(stream1, stream2)) {
                setBufferSize(A_i, j);
                setExhausted(A_i, false);
                (*(A_i + HT_INDEX)) = packHT(0, j);
                return;
            }
//            stream.pop();
        }
        
        // set buffer attributes
        setBufferSize(A_i, size_i);
        setExhausted(A_i, 0);
        
        // pack head and tail
        (*(A_i + HT_INDEX)) = packHT(0, size_i-1);
        
        A_i = getRightChild(A_i);
    }
    
    // STATE: A_i points to the A at link 'linkIndex'
    
   // monom_t* link_i = A_i - LINK_OVERHEAD;       // go to link_i (of linkIndex)
    
    // next, insert in buffers on path from A_i to S_ici
    
    long size_A_i = partOfPSizes.front(); partOfPSizes.pop();
    for (int j = 0; j < size_A_i; j++) {
        (*(A_i + (j + BUFFER_OVERHEAD))) = pop(stream1, stream2); //stream.front();  
        
        // if both streams are empty, then we're done
        if (empty(stream1, stream2)) {
            setBufferSize(A_i, j);
            setExhausted(A_i, false);
            (*(A_i + HT_INDEX)) = packHT(0, j);
            return;
        }
        
//        stream.pop();
    }
    
    
    (*(A_i + HT_INDEX)) = packHT(0, size_A_i-1);
    setBufferSize(A_i, size_A_i);
    setExhausted(A_i, false);
    
    heap_t::iterator link_i = gotoLink(getParent(A_1)-1, (int)linkIndex);

    heap_t::iterator nextBuffer = getLeftChild(A_i);        // point to B_i
    long size_B_i = partOfPSizes.front(); partOfPSizes.pop();
    
    for (int j = 0; j < size_B_i; j++) {
        (*(nextBuffer + (j + BUFFER_OVERHEAD))) = pop(stream1, stream2); //stream.front();  
        
        // if both streams are empty, then we're done
        if (empty(stream1, stream2)) {
            setBufferSize(nextBuffer, j);
            setExhausted(nextBuffer, false);
            (*(nextBuffer + HT_INDEX)) = packHT(0, j);
            incrementNumberOfElementsInLowerBy(link_i, j);
            return;
        }
        
//        stream.pop();
    }
    
    amountInsertedIntoLower += size_B_i;    // inserted size_B_i elements into B_i
    
    (*(nextBuffer + HT_INDEX)) = packHT(0, size_B_i-1);               // set head, tail
    setExhausted(nextBuffer, false);                                // and exhausted of B_i
    setBufferSize(nextBuffer, size_B_i);
    

    long height = linkIndex;
    
    if (linkIndex == 6) height++;
    if (linkIndex == 7) height+=2;

    
	stack<char> path_i;
	getPath(c_i, height, path_i);
    
    for (int i = 0; i < height-1; i++) {
        char step = path_i.top();
        path_i.pop();
        
        if (step == 'l') {
            nextBuffer = getLeftChild(nextBuffer);
        }
        else {
            nextBuffer = getRightChild(nextBuffer);
        }
        
        long buffSize = partOfPSizes.front(); partOfPSizes.pop();
        
        for (int j = 0; j < buffSize; j++) {
            (*(nextBuffer + (BUFFER_OVERHEAD+j))) = pop(stream1, stream2); //stream.front();
            
            // if both streams are empty, then we're done
            if (empty(stream1, stream2)) {
                setBufferSize(nextBuffer, j);
                setExhausted(nextBuffer, false);
                (*(nextBuffer + HT_INDEX)) = packHT(0, j);
                incrementNumberOfElementsInLowerBy(link_i, amountInsertedIntoLower+j);
                return;
            }
            
//            stream.pop();
        }
        
        amountInsertedIntoLower += buffSize;    // inserted buffSize amount into interior node
        
        (*(nextBuffer + HT_INDEX)) = packHT(0, buffSize-1);               // set head, tail
        

        setExhausted(nextBuffer, false);                            // and exhausted of nextBuffer
        setBufferSize(nextBuffer, buffSize);
    }

    // fill the S_ici buffer that was originally empty with the remaining elements
    if (path_i.top() == 'l') {
        nextBuffer = getLeftChild(nextBuffer);
    }
    else {
        nextBuffer = getRightChild(nextBuffer);
    }
    

    if (ASSERTIONS) {
        // 1) Make sure that the number of elements remaining in the stream is 
        //    less than or equal to the capacity of the leaf
        assert(stream1.size()+stream2.size() <= getBufferCapacity(nextBuffer));
    }
    
    long leaf_size = 0;
    while (!empty(stream1, stream2)) {
        //insertInBuffer(nextBuffer, stream.front());
        (*(nextBuffer + (BUFFER_OVERHEAD+leaf_size))) = pop(stream1, stream2); //stream.front();
//        stream.pop();
        leaf_size++;
        
    }
    

    
    (*(nextBuffer + HT_INDEX)) = packHT(0, leaf_size-1);               // set head, tail
    setBufferSize(nextBuffer, leaf_size);                       // set buffer size
    setExhausted(nextBuffer, false);                            // and exhausted of nextBuffer
    
    amountInsertedIntoLower += leaf_size;       // inserted leaf_size elements into leaf
    
    // increment number of elements in the lower part of this link by 'leaf_size'
    incrementNumberOfElementsInLowerBy(link_i, amountInsertedIntoLower);      
}

/*
 *  getPath: returns a stack of 'l' and 'r' characters designating 'left' and 'right' respectively
 *              representing the path from B_i down to leaf c
 *
 *  @param c: the index of the leaf (in link i)
 *  @param height: the height of the currrent tree (from B_i to leaf c)
 */
void getPath(monom_t c, long height, stack<char> &path) {
    monom_t mask = 1;
    
    c--;
    for (int i = 0; i < height; i++) {
        if (((c & mask) >> i) == 0) {
            path.push('l');
        }
        else
            path.push('r');
        
        mask = mask << 1;
    }    
    
}




/*
 *  mergeStep: pops the max from the children of parent and pushes it into the parent
 *
 *  @param parent: a pointer to the parent buffer
 */
void mergeStep (heap_t::iterator parent) {
    // EFFECTS: if a buffer becomes empty as a result of this function, then it is marked
    //          as exahusted
    
    heap_t::iterator leftChild = getLeftChild(parent);
    heap_t::iterator rightChild = getRightChild(parent);
    
    if ((leftChild == I_NULL && rightChild == I_NULL) || (isBufferEmpty(leftChild) && isBufferEmpty(rightChild))) {
        return;
    }

	// initialize both values to 0
	monom_t leftMaxDegree = 0;
	monom_t rightMaxDegree = 0;
	
	// left-right valid show if the values of rightMax and leftMax are 'valid' (depending on whether or not the buffer was empty)
	bool leftValid = false;
	bool rightValid = false;
	
	// get (peek) max from non empty buffers
	if (leftChild != I_NULL && !isBufferEmpty(leftChild)) {
		leftMaxDegree = GET_DEGREE(getMax(leftChild));
		leftValid = true;
	}
	
	if (rightChild != I_NULL && !isBufferEmpty(rightChild)) {
		rightMaxDegree = GET_DEGREE(getMax(rightChild));
		rightValid = true;
	}
    
	// if neither value id valid, don't merge anything
	if (!leftValid && !rightValid) {
		return;
	}
	
	monom_t maxValue = 0;		// will hold the value to be popped from the child holding the max element
	heap_t::iterator chosenBuffer;		// will point to the child buffer that has the max element (and will be popped)
	
	// if both values are valid, take the max
	if (leftValid && rightValid) {
		if (leftMaxDegree >= rightMaxDegree) {		// if the left value is greater (or equal) to the right value, pop value from the left buffer
			chosenBuffer = leftChild;
		}
		else {			// if the right value is greater (or equal) to the left value, pop value from the right buffer
			chosenBuffer = rightChild;
		}
	}
	// if the left value is valid, but the right is not, then pop the value from the left buffer
	else if (leftValid && !rightValid) {
		chosenBuffer = leftChild;
	}
	// if the right value is valid, but the left is not, then pop the value from the right buffer
	else {
		chosenBuffer = rightChild;
	}
	
	// remove the max element from the chosen child and give it to the parent buffer
	maxValue = popMax(chosenBuffer);
	pushOnTail(parent, maxValue);
	
	// if we removed an element from a B-buffer, then we need to decrement the number of elements
	// in the lower part of the current link
	if (getBufferType(chosenBuffer) == B_TYPE) {
    	heap_t::iterator link_i = parent - LINK_OVERHEAD;   // go to the start of current link
		decrementNumberOfElementsInLowerBy(link_i, 1);      // decrement by one
	}

	
	// if we removed an element from B, then we need to decrement the number of elements
    //    // in the lower part of the current link
    //    if (getBufferType(possiblyExhausted) == B_TYPE) {
    //        heap_t::iterator link_i = parent - LINK_OVERHEAD;   // go to the start of current link
    //        
    //        decrementNumberOfElementsInLowerBy(link_i, 1);      // decrement by one
    //    }
	
    // // get max elements of left and right buffers
    //    monom_t leftMax = getMax(leftChild);
    //    monom_t rightMax = 0;
    //    if (rightChild != I_NULL)     // since rightChild can be NULL if parent is A_i
    //        rightMax = getMax(rightChild);
    //    
    //    
    //    heap_t::iterator possiblyExhausted = I_NULL;           // buffer that will possibly be exhausted after pop()
    //    
    //    if (GET_DEGREE(leftMax) > GET_DEGREE(rightMax)) {
    //        // insert left max in parent
    //        pushOnTail(parent, leftMax);
    //        popMax(leftChild);
    //        
    //        possiblyExhausted = leftChild;
    //    }
    //    else {
    //        // insert right max in parent
    //        pushOnTail(parent, rightMax);
    //        popMax(rightChild);
    //        
    //        possiblyExhausted = rightChild;
    //    }
    //    
    //    // if we removed an element from B, then we need to decrement the number of elements
    //    // in the lower part of the current link
    //    if (getBufferType(possiblyExhausted) == B_TYPE) {
    //        heap_t::iterator link_i = parent - LINK_OVERHEAD;   // go to the start of current link
    //        
    //        decrementNumberOfElementsInLowerBy(link_i, 1);      // decrement by one
    //    }
    //    
    //    
    //    // check if it is exhausted
    //    if (isBufferEmpty(possiblyExhausted)) {
    //        if (isLeaf(possiblyExhausted)) { // if isEmpty and isLeaf, then definitely exhausted
    //            setExhausted(possiblyExhausted, true);
    //        }
    //        else {      // if isEmpty but NOT leaf, check if children are exhausted
    //            heap_t::iterator leftPE = getLeftChild(possiblyExhausted);
    //            heap_t::iterator rightPE = getRightChild(possiblyExhausted);
    //        
    //            if (isExhausted(leftPE) && isExhausted(rightPE)) {
    //                setExhausted(possiblyExhausted, true);
    //            }
    //        }
    //    }
    
}


/*
 *  fill: fills the current buffer by recursively filling its children until that buffer is full
 *
 *  @param buffer: a pointer to the buffer that will be filled
 */
void fill (heap_t::iterator buffer) {
    // EFFECTS: if after filling, a child of 'buffer' is empty, then that child
    //          is set to be exhausted
    
    if (isExhausted(buffer)) {
        return;
    }

    heap_t::iterator leftChild = getLeftChild(buffer);
    heap_t::iterator rightChild = getRightChild(buffer);

    // if both children are exhausted AND current buffer is empty, set it as exhausted
    if (isExhausted(leftChild) && isExhausted(rightChild)) {
        if (isBufferEmpty(buffer)) {
            setExhausted(buffer, true);
        }
        
        return;
    }
    
	// fill the buffer
    while (!isBufferFull(buffer)) {
        
        // if the leftChild is not exhausted and is empty, fill it (if it is not a leaf)
        if (!isExhausted(leftChild) && isBufferEmpty(leftChild)) {
            if (isLeaf(leftChild)) {
                setExhausted(leftChild, true);  // set leaf to be exhausted since it is empty
            }
            else
                fill (leftChild);
        }
        
        // if the rightChild is not exhausted and is empty, fill it (if it is not a leaf)
        if (!isExhausted(rightChild) && isBufferEmpty(rightChild)) {
            if (isLeaf(rightChild)) {
                setExhausted(rightChild, true);
            }
            else
                fill (rightChild);
        }
        
        if (isExhausted(leftChild) && isExhausted(rightChild)) {
            break;
        }
        
        mergeStep(buffer);
    }
    
    // assertions
    if (ASSERTIONS) {
        // 1) if buffer is empty and its children are exhausted, then buffer is also exhausted
        assert(!(isBufferEmpty(buffer) && isExhausted(leftChild) && isExhausted(rightChild) && !isExhausted(buffer)));
        
        // 2) if a child is a leaf and empty, then it is exhausted
        assert(!(isLeaf(leftChild) && isBufferEmpty(leftChild) && !isExhausted(leftChild)));
        assert(!(isLeaf(rightChild) && isBufferEmpty(rightChild) && !isExhausted(rightChild)));
    }
    
}

/************************ Elements in Buffer ******************************\
 *
 * The elements in the buffer are sorted in decreasing order from left to right.
 */

/*
 * pushOnTail: pushes the element onto the tail of the buffer
 */
void pushOnTail (heap_t::iterator buffer, monom_t element) {
    long size = getBufferSize(buffer);
    
    if (size == 0) {
        (*(buffer + BUFFER_OVERHEAD)) = element;
        (*(buffer + HT_INDEX)) = packHT(0, 0);
        setBufferSize(buffer, 1);
        return;
    }
    
    long capacity = getBufferCapacity(buffer);
    
    if (size == capacity) {
        return;
    }
    
    long head = UNPACK_HEAD((*(buffer + HT_INDEX)));
    long tail = UNPACK_TAIL((*(buffer + HT_INDEX)));
    newTailIndex(tail, size, capacity);
    
    (*(buffer + (BUFFER_OVERHEAD+tail))) = element;
    setBufferSize(buffer, size+1);    
    (*(buffer + HT_INDEX)) = packHT(head, tail);
}

/*
 * popMax: pops the max element from buffer and returns it
 */
monom_t popMax (heap_t::iterator buffer) {
    long size = getBufferSize(buffer);
    
    
    if (size == 0) {
        return 0;
    }

	long head = UNPACK_HEAD((*(buffer + HT_INDEX)));
	long capacity  = getBufferCapacity(buffer);
    
    monom_t element = getElementAtPosition(buffer, head);
    nextElement(head, capacity);
    size--;
    setBufferSize(buffer, size);
    
    
    (*(buffer + HT_INDEX)) = packHT(head, UNPACK_TAIL((*(buffer + HT_INDEX))));

    if (size == 0) {        
    	if (isExhausted(getLeftChild(buffer)) && isExhausted(getRightChild(buffer))) {
    		setExhausted(buffer, true);
    	}
    }

    return element;
}



#pragma mark -
#pragma mark profile_adaptive_performance

/*
 * getLeafsForPAP: Finds a leaf (in the specified link) that is suitable for the 
 *                profile adaptive performance. The priority is to find an empty 
 *                leaf. If non exist, it finds the two leafs with the minimum
 *                number of elements. Returns the index of the leaf with the 
 *                minimum number of elements (Note: If an empty leaf is found,
 *                the index of that leaf is returned)
 */
int getLeafsForPAP(heap_t::iterator link_i, heap_t::iterator &leaf_1, heap_t::iterator &leaf_2) {
    leaf_1 = I_NULL;
    leaf_2 = I_NULL;
    
    long height = UNPACK_LINK_INDEX((*(link_i + TI_INDEX)));
    
    if (height == 6) {
        height++;
    }
    else if (height == 7) {
        height++;
    }
    
    heap_t::iterator B_i = getLeftChild(link_i+LINK_OVERHEAD);       // goto B_i
    
    stack<char> path;
    
    heap_t::iterator current = B_i;
    
    for (monom_t i = 0; i < height; i++) {       // show path for first leaf
        current = getLeftChild(current);
        path.push('l');
    }
    
    int leafIndex = 1;  // index of first leaf
    
    if (isBufferEmpty(current)) {
        leaf_1 = current;
        return leafIndex;       
    }
    
    heap_t::iterator min1 = current;
    heap_t::iterator min2 = I_NULL;
    
    int min1Index = leafIndex;
    
    // keep looping until we either find an empty buffer, or no buffer is empty, in which case 
    // we return two buffers with the minimum number of elements
    while (true) {
        //current = nextNodeAtHeight(current, height, &path);
        
        current = nextNodeAtDepth(current, height, path);
        
        if (current == I_NULL)    // if no more leafs
            break;
        
        leafIndex++;
        
        if (isBufferEmpty(current)) {
            leaf_1 = current;
            return leafIndex;
        }
        
        
        // keep track of the buffers with the minimum number of elements
        if (min1 == I_NULL) {
            min1 = current;
            min1Index = leafIndex;
        }
        else if (min2 == I_NULL) {    // keep min2 < min1
            
            // if min1 > current, then min1 = current and min2 = min1
            if (getBufferSize(min1) > getBufferSize(current)) { 
                min2 = min1;
                min1 = current;
                min1Index = leafIndex;      // update index of smallest leaf
            }
            else {  // min1 <= current, then min2 = current
                min2 = current;
            }
        }
        else {  // make sure that min1 < min2
            if (getBufferSize(min2) > getBufferSize(current)) {
                
                // if current < min1 < min2, then min2 = min1 and min1 = current
                if (getBufferSize(min1) > getBufferSize(current)) {
                    min2 = min1;
                    min1 = current;
                    min1Index = leafIndex;
                }
                else {  // min1 < current <= min2, then min2 = current
                    min2 = current;                    
                }
            }
        }
    }
    
    leaf_1 = min1;
    leaf_2 = min2;
    
    return min1Index;
}



#pragma mark -
#pragma mark main

// Computes the size needed for buffers in a k-way merger.         
int kSize(long k) {
    
    if (k<=2) {
        return 0; // A BinMerger does need extra buffers.
    } 
    
    double height = log(static_cast<long double>(k))/
    log(static_cast<long double>(2));
    
    int heightInt = static_cast<int>(floor(height + 0.5));
    
    int bottomI = static_cast<int>(heightInt >> 1);
    int topI = heightInt - bottomI;
    
    int topK = 1 << topI;                 // = 2^(topI)
    int bottomK = 1 << bottomI;        // = 2^(bottomI) 
    
    int sizeOfBuffers = static_cast<int>(ceil(pow(k,1.5)));
    
    
    int totalMidSize = topK * sizeOfBuffers;
    
    // The size is the middlebuffers + the size of the top-merger + the
    // size of the topK bottomMergers.
    return totalMidSize + kSize(topK) + (topK * kSize(bottomK));
}



/*
 * nextNodeAtHeight: returns the node that comes right after 'current' at the same level
 *
 * Precondition: 
 *      - state: for first call, 'state' contains 'l' character "height-1" times. For 
 *                  subsequent calls, 'state' is that of the previous call
 *      - current: points to current buffer
 *
 */
heap_t::iterator nextNodeAtHeight (heap_t::iterator current, long height, stack<char> &state) {
    
    if (height < 1) {          // has no nodes, so current stays the same
        return I_NULL;
    }
    
    state.pop();       // pop the current buffer
    current = getParent(current);
    
    while (!state.empty()) {
        
        char top = state.top();        // see state of parent to know where to go next
        
        if (top == 'l') {       // if left, go to right
            
            
            current = getRightChild(current);
            state.pop();
            state.push('r');
            
            // go to leftmost
            while (state.size() != height-1) {
                current = getLeftChild(current);
                state.push('l');
            }
            
            if (state.size() == height-1) {
                
                state.push('n');
                return current;
            }
            
        }
        else if (top == 'r') {      // if right, go up
            current = getParent(current);
            state.pop();
            continue;
        }
        
        
    }
    
    return I_NULL;        // if all nodes at that level have been visited
}

heap_t::iterator getNodeAtLevel (heap_t::iterator B_i, int nodePos, long level) {
	stack<char> path;
	getPath(nodePos, level, path);
    
    long pathSize = path.size();
    heap_t::iterator curr = B_i;
    
    for (int i = 0; i < pathSize; i++) {
        char step = path.top();
        path.pop();
        
        if (step == 'l') {
            curr = getLeftChild(curr);
        }
        else {
            curr = getRightChild(curr);
        }
    }
    
    return curr;
}


/*
 * nextNodeAtDepth: 
 *          For any link i, this method returns a pointer to the next node at
 *          a certain 'depth'. Note: this method is meant to be called several 
 *          times sequentially so that at each 'return', the next node of that
 *          depth is returned.
 *
 *          The first time this method is called (in a related sequence of calls),
 *          'current' should point to the first node at 'depth', and 'state'
 *          should contain the character 'l' 'depth' times.
 *          After that, every subsequent call should contain the 'state' variable
 *          (as modified by the previous called), the same 'depth', and 'current'
 *          should point to the returned node of the previous call.                                
 *
 */
heap_t::iterator nextNodeAtDepth (heap_t::iterator current, long depth, stack<char> &state) {
    if (depth == 0) {
        return I_NULL;        // the only node is B_i
    }
    
    while (!state.empty()) {
        
        char top = state.top();        // see state of parent to know where to go next
        
        // if last move was 'left', then go to right sibling
        if (top == 'l') {       
            current = getParent(current);       // go to parent first
            current = getRightChild(current);   // then go to right child
            
            // replace the 'l' with 'r' to indicate right movement
            state.pop();
            state.push('r');
            
            // go to leftmost child
            while (state.size() != depth) {
                current = getLeftChild(current);
                state.push('l');
            }
            
            return current;
            
            
        }
        else if (top == 'r') {      // if right, go up
            current = getParent(current);
            state.pop();
            continue;
        }
        
        
    }
    
    return I_NULL;

}


// TESTING

bool testPackingAndUnpacking (bool printLog) {
    
    bool worksForAll = true;
    
    // test ti
    long totalLinks = 5;
    long linkIndex = 3;
    
    monom_t ti = packTI(totalLinks, linkIndex);
    
    long unpackedTotalLinks = UNPACK_TOTAL_LINKS(ti);
    long unpackedLinkIndex = UNPACK_LINK_INDEX(ti);
    
    if ((totalLinks != unpackedTotalLinks) || (linkIndex != unpackedLinkIndex)) {
        worksForAll = false;
    }
    
    
    if (printLog)
        printf("TI:  Was %lu, %lu.... is %lu, %lu\n", totalLinks, linkIndex, unpackedTotalLinks, unpackedLinkIndex);
    
    
    // test sce
    long size = 5;
    long capacity = 10;
    long exhausted = 1;
    long isLeaf = 1;
    
    
    monom_t sce = packSCLE(size, capacity, isLeaf, exhausted);
    
    long unpackedSize = UNPACK_SIZE(sce);
    long unpackedCapacity = UNPACK_CAPACITY(sce);
    long unpackedExhausted = UNPACK_EXHAUSTED(sce);
    
    if ((size != unpackedSize) || (capacity != unpackedCapacity) || (exhausted != unpackedExhausted))
        worksForAll = false;
    
    if (printLog)
        printf("SCE: Was %lu, %lu, %lu.... is %lu, %lu, %lu\n", size, capacity, exhausted, unpackedSize, unpackedCapacity, unpackedExhausted);
    
    
    // test hi
    long height = 6;
    long bufferIndex = 17;
    
    monom_t hi = packHI(height, bufferIndex);
    
    long unpackedHeight = UNPACK_HEIGHT(hi);
    long unpackedBufferIndex = UNPACK_BUFFER_INDEX(hi);
    
    if ((height != unpackedHeight) || (bufferIndex != unpackedBufferIndex)) 
        worksForAll = false;
    
    if (printLog)
        printf("HI: Was %lu, %lu.... is %lu, %lu\n", height, bufferIndex, unpackedHeight, unpackedBufferIndex);
    
    return worksForAll;
}


void testTreeCapacities (heap_t::iterator B_i, long linkIndex) {
    printf("TREE %lu:\n", linkIndex);
    
    // print root
    printf("B_i:\t\t Capacity = %lu\n", UNPACK_CAPACITY((*(B_i + SCLE_INDEX))));
    
    heap_t::iterator currentBuffer = B_i;
    
    long height = log2((long) pow(2.0, (double) linkIndex));
    
    for (int levelCounter = 1; levelCounter < height; levelCounter++) {
        long numOfChildren = (long) pow(2.0, levelCounter);
        
        printf("\nLEVEL %d:\n", levelCounter);
        
        for (int j = 1; j <= numOfChildren; j++) {
            currentBuffer = getNodeAtLevel(B_i, j, levelCounter);
            
            
            
            printf("NODE %d:\t\t Capacity = %lu\n", j, UNPACK_CAPACITY((*(currentBuffer + SCLE_INDEX))));
            
        }        
    }
    
    // print leafs
    printf("\nLeafs:\n");
    long numOfLeafs = (long) pow(2.0, (double) height);
    
    for (int i = 1; i <= numOfLeafs; i++) {
        currentBuffer = getNodeAtLevel(B_i, i, height);
        
        printf("S %d:\t\t Capacity = %lu\n", i, UNPACK_CAPACITY((*(currentBuffer + SCLE_INDEX))));
    }
    
}




/*
 * printBuffer: prints current state of buffer
 */
void printBuffer (heap_t::iterator buffer) {
    printf("\t Capacity   = %lu\n", getBufferCapacity(buffer));
    printf("\t Left       = %llu\n", (*(buffer + LEFT_CHILD_INDEX)));
    printf("\t Right      = %llu\n", (*(buffer + RIGHT_CHILD_INDEX)));
    printf("\t Parent     = %llu\n", (*(buffer + PARENT_INDEX)));
    printf("\t Exhausted  = %s\n", isExhausted(buffer) ? "true" : "false");
    printf("\t Size       = %lu\n", getBufferSize(buffer));
}


void printKMerger (heap_t::iterator B_i, long height) {
    //long kq = getBufferCapacity(B_i);
    //long k_i = (long) pow((double) kq, 1.0/3.0);
    //long linkIndex = log2(k_i);
    
        
    heap_t::iterator currentBuffer = B_i;
    
    //long height = log2(pow(2, linkIndex));
    
    for (int levelCounter = 1; levelCounter < height; levelCounter++) {
        long numOfChildren = (long) pow(2.0, (double) levelCounter);
        
        printf("\nLEVEL %d:\n", levelCounter);
        
        for (int j = 1; j <= numOfChildren; j++) {
            currentBuffer = getNodeAtLevel(B_i, j, levelCounter);
            
            printf("Node: %d\n", j);
            printBuffer(currentBuffer);     // print the state of the buffer
            
        }        
    }
}

void printLeafs (heap_t::iterator B_i, long linkIndex) {
   // long k_i = pow(2, linkIndex); //(long) pow(getBufferCapacity(B_i), 1/3.0);
    //long linkIndex = log2(k_i);
    
    long height = log2((long) pow(2.0, (double) linkIndex));
    
    heap_t::iterator currentBuffer = B_i;
    // print leafs
    printf("\nLeafs:\n");
    long numOfLeafs = (long) pow(2.0, (double) height);
    
    for (int i = 1; i <= numOfLeafs; i++) {
        currentBuffer = getNodeAtLevel(B_i, i, height);
        
        printf("Leaf: %d\n", i);
        printBuffer(currentBuffer);
    }
}


void printLink (heap_t::iterator link_i) {
    
    printf("Link:   ELC = %llu\n", getEmptyLeafCounter(link_i));
    
    heap_t::iterator A_i = link_i + LINK_OVERHEAD;
    printf("A_i:\n"); printBuffer(A_i);
    
    heap_t::iterator B_i = getLeftChild(A_i);
    printf("B_i:\n"); printBuffer(B_i);
    
    printf("\n");
    
    
    printKMerger(B_i, UNPACK_LINK_INDEX((*(link_i + TI_INDEX))));
    
    printf("\n");
    
    printLeafs(B_i, UNPACK_LINK_INDEX((*(link_i + TI_INDEX))));
    
}


void printBufferElements (heap_t::iterator buffer) {
    long index = UNPACK_HEAD((*(buffer + HT_INDEX)));
    //long end = UNPACK_TAIL(buffer[HT_INDEX]);
    monom_t size = getBufferSize(buffer);
    monom_t capacity = getBufferCapacity(buffer);
    
    
    
    printf("[");
    
    
    for (monom_t i = 0; i < size; i++) {
        if (i == 0) {
            printf("%llu", GET_DEGREE(getElementAtPosition(buffer, index)));
        }
        else
            printf(",%llu", GET_DEGREE(getElementAtPosition(buffer, index)));
        
        nextElement(index, capacity);
    }
    printf("]\n");
}



void printQueueElements (heap_t &queue) {
    heap_t::iterator I = queue.begin() + 1;
    printf("I: ");  printBufferElements(I);         // print elements of I
    
    heap_t::iterator A_i = getLeftChild(I);
    int i = 1;
    while (A_i != I_NULL && !isExhausted(A_i)) {
        
        printf("\n\nLink %d:\n\n", i);
        
        // print elements of link i
        printf("A_%d: ", i); printBufferElements(A_i);
        
        heap_t::iterator B_i = getLeftChild(A_i);
        printf("B_%d: ", i); printBufferElements(B_i);
        
        printKMergerElements(B_i, i);
        printLeafElements(B_i, i);
        
        
        A_i = getRightChild(A_i);
        i++;
    }

	printf("\n\n");
}

void printKMergerElements (heap_t::iterator B_i, long height) {    
    
    heap_t::iterator currentBuffer = B_i;
    
    for (int levelCounter = 1; levelCounter < height; levelCounter++) {
        long numOfChildren = (long) pow(2.0, (double) levelCounter);
        
        printf("\nLEVEL %d:\n", levelCounter);
        
        for (int j = 1; j <= numOfChildren; j++) {
            currentBuffer = getNodeAtLevel(B_i, j, levelCounter);
            
            printf("Node %d: ", j);
            printBufferElements(currentBuffer);     // print the buffer elements
            
        }        
    }
}

void printLeafElements (heap_t::iterator B_i, long height) {

    
    heap_t::iterator currentBuffer = B_i;
    // print leafs
    printf("\nLeafs:\n");
    long numOfLeafs = (long) pow(2.0, (double) height);
    
    for (int i = 1; i <= numOfLeafs; i++) {
        currentBuffer = getNodeAtLevel(B_i, i, height);
        
        printf("Leaf %d: ", i);
        printBufferElements(currentBuffer);
    }
}




bool printProbBuffExhausted(ofstream &stream) {
   
        return false;
    
    if (isLeaf(prob_buffer)) {
        cout << "Its a leaf!" << endl;
        exit(1);
    }

    if (!isExhausted(getLeftChild(prob_buffer))) {
        cout << "left_size = " << getBufferSize(getLeftChild(prob_buffer)) << endl;
    }
    
    return true;
}



void printProbBuffer() {
    if (prob_buffer !=I_NULL) 
        printBuffer(prob_buffer);
}



void enableProbBufferCheck() {
    check_for_prob = true;
}

void assertExhaustedTargetBuffer() {
    if (isExhausted(prob_buffer))
        assert(isBufferEmpty(prob_buffer));
}

void printElementsInIAndA1(heap_t::iterator I) {
    printf("I:\n");printBufferElements(I);
    printf("\n\nA1:\n");    printBufferElements(getLeftChild(I));
    
}

void printQueueElements(heap_t::iterator q) {
	heap_t::iterator I = q + 1;
    printf("I: ");  printBufferElements(I);         // print elements of I
    
    heap_t::iterator A_i = getLeftChild(I);
    int i = 1;
    while (A_i != I_NULL && !isExhausted(A_i)) {
        
        printf("\n\nLink %d:\n\n", i);
        
        // print elements of link i
        printf("A_%d: ", i); printBufferElements(A_i);
        
        heap_t::iterator B_i = getLeftChild(A_i);
        printf("B_%d: ", i); printBufferElements(B_i);
        
        printKMergerElements(B_i, i);
        printLeafElements(B_i, i);
        
        
        A_i = getRightChild(A_i);
        i++;
    }

	printf("\n\n");
}

bool alreadyPrinted = false;

void printQueueState() {
	if (!alreadyPrinted) {
		alreadyPrinted = true;
		printQueueElements(myQueue.begin());
	}
}
