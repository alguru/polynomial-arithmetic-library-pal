//
//  Funnel Heap
//  Funnel Heap with ID
//
//  Created by Karl Gemayel on 16/05/2012.
//  Copyright 2012 American University Of Beirut. All rights reserved.
//

/* 
 *  CO Priority Queue simulates a cache-oblivious priority queue, where elements 
 *  are extracted in non-increasing order. Note that the queue does not support
 *  negative elements.
 *
 *  Usage:
 *      1) Initilize the queue using initializeQueue (int numOfLinks)
 *      2) Insertion should be done using insert (monom_t *queue, monom_t element)
 *      3) Extraction should be done using deleteMax(monom_t* queue)
 */

/** 
 * \class FunnelHeap
 * \brief A Max Heap implementation using the Funnel Heap design.
 *
 * The Funnel Heap is list of consecutive links, where each link contains its own K-Funnel,
 * whose properties depend on the index of the link. The K-Funnel is a binary tree-like 
 * structure whose nodes represent mergers, and edges and leafs represent buffers. Each buffer
 * stores a sorted sequence of elements, and therefore invoking a 'fill' operation on the 
 * root would fill that root with a sorted sequence of the elements present in the tree. 
 * Consecutive links are also connected together with mergers, which would result in a sorted
 * extraction from the whole queue when we wish to extract elements.
 */

#ifndef FunnelHeap_h
#define FunnelHeap_h

#include "Heap.h"
#include "Monomial.h"
#include <vector>

using namespace std;

/** 
 * \file FunnelHeap.h
 * \typedef define the type of the heap 
 */
typedef vector<monom_t> heap_t;

class FunnelHeap : public Heap {
private:
	int numOfLinks;		/**< number of links */
	heap_t heap;		/**< the actual heap itself */
	
public:
	/**
	 * A constructor that initializes the heap with the specified number of links
	 *
	 * \param numOfLinks the number of links
	 */
	FunnelHeap(int numOfLinks);
	
	/**	
	 * Inserts an element that has a degree, coefficient, and an id into the heap
	 * in its correct position so as to not disturb the max-heap property.
	 *
	 * \param degree the degree of the element
	 * \param coef the coefficient of the element
	 * \param the id of the element
	 */
	virtual void insert (deg_t degree, coef_t coef, ID_t f_id);
	
	/** 
	 * Extracts the max element of the heap and returns it.
	 *
	 * \return The value of the max element
	 */
	virtual monom_t poll ();
	
	/** 
	 * Returns the value of the max element of the heap without actually extracting it.
	 *
	 * @return The value of the max element 
	 */
	virtual monom_t peek();
	
	/** 
	 * Returns true if the heap is empty, and false otherwise
	 *
	 * \return if heap is empty - true. Otherwise - false
	 */
	virtual bool isEmpty();
	
	/**
	 * Returns the size of the heap.
	 *
	 * \return The size of the heap.
	 */
	virtual size_t size ();
	
	void printQueueElements ();
	
	
};

#endif
