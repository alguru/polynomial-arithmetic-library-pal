//
//  main.cpp
//  PAL v1.0
//
//  Created by Karl Gemayel on 12/3/11.
//  Copyright (c) 2011 American University of Beirut. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>
#include <sstream>
#include <time.h>
#include "PolynomialOperations.h"

using namespace std;

void usage();
void readArgs(int argc, char *argv[]);

/****************************************/

// arguments from main
string inputName = "mult.in";
bool writeMultiplicationOutputToFile = false;

// global variables for manipulating user-defined options (ex: verbose mode)
//extern bool VERBOSE = false;

int main (int argc, char * argv[]) {

	// read/update arguments
	readArgs(argc, argv);
    
    ifstream in(inputName.c_str());
    string line;
    
	
    getline(in, line);
    int numOfPairs = atoi(line.c_str());
    
    vector<poly_t> f_polynomials (numOfPairs);
    vector<poly_t> g_polynomials (numOfPairs);
    
    poly_t f, g;
    
	// start the timer
	time_t start_t, end_t;
	start_t = time(NULL);	// record the time that the task begins
	
    // loop over input file and collect all pairs

	// read the f_polynomials
    for (int i = 0; i < numOfPairs; i++) {
        
        // read the f polynomial of the i'th pair
        getline(in, line);
        line = line.substr(line.find("=") + 1);
        parsePolynomialString(line, f);
                
        
        // add f to its corresponding vectors
        f_polynomials[i] = f;

		string f_string;
		toString(f, f_string);
		
		// reset the polynomials to read new ones
		f.clear();
    }

	// read the g_polynomials
    for (int i = 0; i < numOfPairs; i++) {
	
        // read the g polynomial of the i'th pair
        getline(in, line);
        line = line.substr(line.find("=") + 1);
        parsePolynomialString(line, g);
        
        
        // add g to its corresponding vectors
        g_polynomials[i] = g;

		string g_string;
		toString(g, g_string);
		
		// reset the polynomial to read a new one
		g.clear();
    }

	poly_t result;
	multiplyMultiplePairs(f_polynomials, g_polynomials, result);
    
	end_t = time(NULL);	// recrod the time that the task ends
    


	// compute elapsed time
	cout << "Time = " << difftime(end_t, start_t) << endl;   

	// open a file to write the execution time to
	ofstream timesFile;
	timesFile.open ("time_max_heap.txt", ios::out | ios::app);
	
	// write the input size with the time
	timesFile << numOfPairs << ": " << difftime(end_t, start_t) << endl;
	
	// close the times file
	timesFile.close();
	
	// write the multiplication output to the output file
	if (writeMultiplicationOutputToFile) {
		string result_string;
	    toString(result, result_string);
	
		string outputFilename = "mult.out";
		
		ofstream mult_output;
		mult_output.open (outputFilename.c_str(), ios::out | ios::app);
		mult_output << result_string.c_str() << endl;
		mult_output.close();
	}
}

// read/update the arguments for running the test case
void readArgs(int argc, char *argv[]) {
	// if there are no arguments (except for the program name), then print usage
	if (argc == 1) 
		usage();	
		
	bool choseOneHeapType = false;
	bool choseInputFile = false;
	
	// if there are arguments, read them
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-i") == 0) {	// choosing input file
			inputName = argv[++i];
			choseInputFile = !choseInputFile;
		}
		else if (strcmp(argv[i], "-v") == 0) {
//			PAL_VERBOSE = true;
		}
		else if (strcmp(argv[i], "-o") == 0) {
			writeMultiplicationOutputToFile = true;
		}
	}
	
	// if any "required" parameter was not given, then print usage
	if (!(choseInputFile)) 
		usage();
}

void usage() {
	cerr << "usage: Optimized Summation of Polynomial Multiplications" << endl <<endl;

	cerr << "OSPM Driver: ospm -i (input_file)" << endl;
	cerr << "Where:  -i (input_file) = name of input file" << endl << endl;
	
	cerr << "Optional Arguments:" << endl;
	cerr << "\t-v: puts the program in verbose mode (prints information)" << endl;
	cerr << "\t-o: print the output to a file (file is named depending on heap used)" << endl;

	// quit program
	exit(0);
}
