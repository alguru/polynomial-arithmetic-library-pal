# This makefile builds the PAL application

CC=g++
CREATEEXE=$(CC) -o $@ $^


all: pal

# BinaryHeap
BinaryHeap: BH_driver.cpp BinaryHeap.o
	$(CREATEEXE)

BinaryHeap.o: BinaryHeap.cpp BinaryHeap.h Monomial.h Heap.h
	$(CC) $(CFLAGS) -c BinaryHeap.cpp

# FunnelHeap
FunnelHeap: FH_driver.cpp FunnelHeap.o
	$(CREATEEXE)

FunnelHeap.o: FunnelHeap.cpp FunnelHeap.h Monomial.h Heap.h
	$(CC) $(CFLAGS) -c FunnelHeap.cpp

# PolynomialOperations
pal: PAL.cpp FunnelHeap.o PolynomialOperations.o
	$(CREATEEXE)

PolynomialOperations.o: PolynomialOperations.cpp PolynomialOperations.h
	$(CC) $(CFLAGS) -c PolynomialOperations.cpp


clean:
	rm -f *.o BinaryHeap FunnelHeap pal

tests: small_tests medium_tests large_tests