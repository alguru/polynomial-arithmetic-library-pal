#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>
#include <time.h>
#include <queue>
#include "FunnelHeap.h"
#include "Monomial.h"

// set the following macro to 'true' if you want to test the results
// of this priority queue to that located in the C++ Standard Library
#define TEST_RESULTS true

// uncomment the #define VERBOSE line if you want the program to 
// print its progress as it goes
#define VERBOSE 


using namespace std;

int* readNumbers(string list, int size);
void genTestFile(char *filename, int numOfOperations);
void useControlHeap (char *inputName, char *outputName);


int main (int argc, char *argv[]) {
	
#ifdef VERBOSE
	// print a start statement
	cout << "Starting execution..." << endl;
#endif
	    
	// generate a test file
   	int N = 10;		// size of the heap
	if (argc > 1) {
		N = atoi(argv[1]);
	}
	    
    size_t elementsInHeap = 0;

    
    size_t cc = 0;
	
	// setup the random generator
	const int UPPER_LIMIT = 1000;
	srand((int)time(NULL));
	int num;
	
	// init heap and perform operations
	// heap_t heap;
	// initializeHeap(heap, N);
	
	FunnelHeap heap (6);
	
#ifdef VERBOSE
	// print that heap allocation is done
	cout << "Heap has been allocated..." << endl;
#endif
	
	// allocate space for the C++ priority queue (used if TEST_RESULTS is set to true)
	priority_queue<int> cpp_q;
	bool correct_results = true;
	
	// start the timer
	time_t start_t, end_t;
	start_t = time(NULL);	// record the time that the task begins
	
    float progress = 0;
	int numOfOperations = 3 * N;		// total number of operations

	// do N inserts
	for (unsigned int i = 0; i < N; i++) {    

#ifdef VERBOSE
        // print progress
        size_t r = 10;
        if (cc % r == 0) {     // if cc is a multiple of r
            float newProgress = (cc / (double) numOfOperations) * 100;
            if (newProgress - progress > 5) {
                progress = newProgress;
                printf("%.0f%% DONE\n", progress);
            }
            
            if (cc == r * 10)
                r = cc;
        }
#endif
        
		// do an insert in our priority queue
		num = (int) (rand() % UPPER_LIMIT) + 1;       // generate a number to insert	
		heap.insert(num, 1, 0);
		
		// do an insert in C++ queue (if TEST_RESULTS = true)
		if (TEST_RESULTS) {
			cpp_q.push(num);
		}
        
        cc++;
    }

#ifdef VERBOSE
	cout << "N/2 deletes coming up :)" << endl;
#endif

	monom_t deletedElem = 0;		// keeps track of the last deleted element from our priority queue
		
	// do N/2 deletes
	for (unsigned int i = 0; i < N/2; i++) {

#ifdef VERBOSE
		// print progress
        size_t r = 10;
        if (cc % r == 0) {     // if cc is a multiple of r
            float newProgress = (cc / (double) numOfOperations) * 100;
            if (newProgress - progress > 5) {
                progress = newProgress;
                printf("%.0f%% DONE\n", progress);
            }
            
            if (cc == r * 10)
                r = cc;
        }
#endif

		// do a delete from our priority queue
		deletedElem = heap.poll();
		
		// do a delete from C++ queue (if TEST_RESULTS = true)
		if (TEST_RESULTS) {
			// if the deleted elements do not match, set correct_results to false
			if (GET_DEGREE(deletedElem) != cpp_q.top()) {
				correct_results = false;
			}
			
			// remove the element
			cpp_q.pop();
		}
		
		cc++;
		
		

	}
	
	// do N/2 inserts
	for (unsigned int i = 0; i < N/2; i++) {
		
#ifdef VERBOSE
		// print progress
        size_t r = 10;
        if (cc % r == 0) {     // if cc is a multiple of r
            float newProgress = (cc / (double) numOfOperations) * 100;
            if (newProgress - progress > 5) {
                progress = newProgress;
                printf("%.0f%% DONE\n", progress);
            }
            
            if (cc == r * 10)
                r = cc;
        }
#endif

		num = (int) (rand() % UPPER_LIMIT) + 1;       // generate a number to insert
		heap.insert(num, 1, 0);
		
		// do an insert in C++ queue (if TEST_RESULTS = true)
		if (TEST_RESULTS) {
			cpp_q.push(num);
		}
		
		cc++;
	}
	
	// do N deletes
	for (unsigned int i = 0; i < N; i++) {
		
#ifdef VERBOSE
		// print progress
        size_t r = 10;
        if (cc % r == 0) {     // if cc is a multiple of r
            float newProgress = (cc / (double) numOfOperations) * 100;
            if (newProgress - progress > 5) {
                progress = newProgress;
                printf("%.0f%% DONE\n", progress);
            }
            
            if (cc == r * 10)
                r = cc;
        }
#endif

		deletedElem = heap.poll();
		
		// do a delete from C++ queue (if TEST_RESULTS = true)
		if (TEST_RESULTS) {
			// if the deleted elements do not match, set correct_results to false
			if (GET_DEGREE(deletedElem) != cpp_q.top()) {
				correct_results = false;
			}
			
			// remove the element
			cpp_q.pop();
		}
		
		cc++;
	}
	
	end_t = time(NULL);	// recrod the time that the task ends
	
#ifdef VERBOSE
	// compute elapsed time
	cout << "Time = " << difftime(end_t, start_t) << endl;
#endif
 
	// open a file to write the execution time to
	ofstream timesFile;
	timesFile.open ("time.txt", ios::out | ios::app);
	
	// write the input size with the time
	timesFile << N << ": " << difftime(end_t, start_t) << endl;
	
	// close the times file
	timesFile.close();
	
	// check if results are correct
	if (TEST_RESULTS) {
#ifdef VERBOSE
		if (correct_results) {
			cout << "The priority queue is working properly!" << endl;
		}
		else {
			cout << "Error: Priority queue results did not match those of C++ priority queue!" << endl;
		}
#endif
	}
	
	return 0;
}


int* readNumbers(string list, int size) {
	int* numbers = new int[size];
	string num;
    stringstream stream(list);
	int i = 0;
    while(i < size && getline(stream, num, ',') ) {
		numbers[i++] = atoi(num.c_str());
	}
		
	return numbers;
}


#include <queue>

void useControlHeap (char *inputName, char *outputName) {
    ifstream in(inputName);
    ofstream out(outputName);
    string line;
    
    if (!in.is_open()) {        // file did not open
        cout << "ERROR: Could not open input file!" << endl;
        return;
    }
    
    priority_queue<int> q;
    
	// read the number of operations that will be performed
	getline(in, line);
	unsigned int numOfOperations = atoi(line.c_str());

    // execute operations on heap
	for (unsigned int i = 0; i < numOfOperations; i++) {     // while did not reach end of file
        getline(in, line);
        
        if (line[0] == 'i') {   // an insert operation
            int num = atoi (line.substr(2).c_str());
            //insert(heap, num, 1, 0);
            q.push(num);
            
            out << "Inserting: " << num << endl;
        }
        else {      // a remove operation
            if (q.empty()) {
                out << "Removing: 0" << endl;
            }
            else {
                out << "Removing: " <<  q.top() << endl;;
                q.pop();
            }
        }
        
    }
    
    out.close();
    in.close();
    
}
