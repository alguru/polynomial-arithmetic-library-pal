#include <iostream>
#include <sstream>
#include <string>
#include <assert.h>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include "FunnelHeap.h"
#include "BinaryHeap.h"
#include "Heap.h"
#include "PolynomialOperations.h"

#define ASSERTIONS false

using namespace std;

int p = 113;                      // prime 
void init(int prime) {
    p = prime;
}

// function prototypes
void heapChooser (size_t numOfPairs, size_t heap_cap);
inline void skipSpaces (char &current, size_t &index, string line);
void getMaxFromAllPairs (int ** &g_i_s, monom_t description [], vector<poly_t*> &f_polynomials, vector<poly_t*> &g_polynomials);
//int getNumberOfMonomials (const char *f);

/* FUNCTION IMPLEMENTATIONS */

// multiply multiple pairs
void multiplyMultiplePairs(std::vector<poly_t> &f_polynomials, std::vector<poly_t> &g_polynomials, poly_t &result) {
	/*
	 * General Code Sketch:
	 *
	 *  1)  Let S = n_1 + n_2 + ... + n_k, where n_i is the number of 
     *      monomials in f_i (for 1 <= i <= k).
     *  2)  Insert the S maximum products of monomials (from all (f_i, g_i) 
     *      pairs) into the heap.
     *  3)  Repeat until all pairs have been fully traversed:
     *          i)  Extract the maximum elemement from the heap
     *              and place it in the result
     *         ii)  Insert the next maximum element into the heap
     *  4)  When all pairs have been traversed, extract all elements
     *      from the max heap and place them in the result
	 */
    

    size_t numOfPairs = g_polynomials.size();
    
	// get maximum number of multiplications that will be done 
	size_t maxMultiplications = 0;
	for (int i = 0; i < numOfPairs; i++) {
		maxMultiplications += f_polynomials[i].size() * g_polynomials[i].size();
	}
    
    size_t currentMultiplications = 0;		// used to track the number of multiplications already done
	

    // get the max number of monomials in the f's, and the total heap capacity
	size_t heap_cap = 0;	// capacity of heap
	int f_max = 0;
	for (int i = 0; i < numOfPairs; i++) {
		heap_cap += f_polynomials[i].size();
		if (f_polynomials[i].size() > f_max) {
			f_max =(int) f_polynomials[i].size();
		}
	}
	

	// choose which heap to use
	heapChooser(numOfPairs, heap_cap);
	
#ifdef USE_BINARY_HEAP
	BinaryHeap A (heap_cap);
#else
	FunnelHeap A (6);
#endif
    
    monom_t f_element, g_element;    // represent an element of f and g
	
	unsigned long long maxNumberOfElementsInHeap = 0;
	

	cout << "Starting the first set of multiplications..." << endl;

	
	// Insert (f1_1*g1_1, f1_2*g1_2,.... f1_n*g1_n).....(fn_1*gn_1....fn_n*gn_n) into the heap
	int id = 0;
	for (int i = 0; i < numOfPairs; i++) {			// loop on all polynomials
		
		poly_t f = f_polynomials[i];
		poly_t g = g_polynomials[i];
		
		for (int j = 0; j < f_polynomials[i].size(); j++) {		// loop on monomials of i'th polynomial
            f_element = f[j];
			g_element = g[0];
            
            coef_t coef = GET_COEF(f_element) * GET_COEF(g_element);
            deg_t degree = GET_DEGREE(f_element) + GET_DEGREE(g_element);
			
			A.insert(degree, coef, id);
			
			maxNumberOfElementsInHeap++;
			id++;
			
			currentMultiplications++;
		}
		
		if (f_polynomials[i].size() < f_max) {			// go to next level in record table
			id += f_max-f_polynomials[i].size();
		}
	}
	
	// print the max number of elements in the heap at any one time
	printf("The max number of elements in the heap is: %llu\n", maxNumberOfElementsInHeap);
	
	/*
	 * The following creates an array of linked lists to reference all monomials of f_i. This is
	 * changed from the previously 2D matrix to save on memory usage. Note that this is important
	 * when dealing with sparse polynomials.
	 */
	
	struct ID {
		int g_counter;
		ID *next;
	};
	
	int n_f = (int) numOfPairs;
	ID *recArray [n_f];
	for (int i = 0; i < n_f; i++) {
		if (f_polynomials[i].size() > 0) {		// check that it has at least one monomial
			recArray[i] = new ID;			
		}
		else {
			continue;
		}
		
		
		ID *current = recArray[i];
		current->g_counter = 0;
		current->next = NULL;
		
		for (int j = 1; j < f_polynomials[i].size(); j++) {		// create link for every monomials of f_i
			current->next = new ID;
			current = current->next;
			current->g_counter = 0;			// added new
			current->next = NULL;
		}
	}
	

	cout << "Starting with second set dealing with insertions/extractions..." << endl;
	
	while (currentMultiplications < maxMultiplications) {
		monom_t max = A.poll();
        
		
		if (max == 0) {
			break;
		}
		
		ID_t maxID = GET_ID(max);
		
		
		
		// increment recTable of f(id)
		int maxRow = (int) (maxID / f_max);
		int maxCol = (int) (maxID % f_max);
		
		// find current reference and update g_counter
		ID *maxRef = recArray[maxRow];
		for (int i = 0; i < maxCol; i++) {
			maxRef = maxRef->next;
		}
		
		if (maxRef->g_counter == g_polynomials[maxRow].size() - 1) 
			maxRef->g_counter = -1;
		else
			maxRef->g_counter++;
		
		coef_t maxCoef = GET_COEF(max);
		deg_t maxDegree = GET_DEGREE(max);
		
        if (maxRef->g_counter != -1) {
			// add the successor
			poly_t f = f_polynomials[maxRow];
			poly_t g = g_polynomials[maxRow];
			
			//			int g_successor = recTable[maxRow][maxCol];
			int g_successor = maxRef->g_counter;
            
            f_element = f[maxCol];
            g_element = g[g_successor];
            
            coef_t coef = GET_COEF(f_element) * GET_COEF(g_element);
            deg_t degree = GET_DEGREE(f_element) + GET_DEGREE(g_element);
			
			
			if (coef % p != 0) {
				A.insert(degree, coef, maxID);
			}			
			currentMultiplications++;
		}
        
		// check if max of heap has degree equal to maxDegree
		
		monom_t current = A.peek();
		while (!A.isEmpty()) {                              // REVIEW: was current != NULL
			if (GET_DEGREE(current) == maxDegree) {
				
				current = A.poll();
                				
				ID_t currentID = GET_ID(current);
				
				int currentRow = (int) (currentID / f_max);
				int currentCol = (int) (currentID % f_max);
				
				// find current reference and update g_counter
				ID *currentRef = recArray[currentRow];
				for (int i = 0; i < currentCol; i++) {
					currentRef = currentRef->next;
				}
				
				if (currentRef->g_counter == g_polynomials[currentRow].size() - 1) 
					currentRef->g_counter = -1;
				else
					currentRef->g_counter++;
				
				
				if (currentRef->g_counter != -1) {
					// add the successor
					poly_t f = f_polynomials[currentRow];
					poly_t g = g_polynomials[currentRow];
					
					//int g_successor = recTable[currentRow][currentCol];
					int g_successor = currentRef->g_counter;
                    
                    f_element = f[currentCol];
                    g_element = g[g_successor];
                    
                    coef_t coef = GET_COEF(f_element) * GET_COEF(g_element);
                    deg_t degree = GET_DEGREE(f_element) + GET_DEGREE(g_element);
                    
					A.insert(degree, coef, currentID);
					
					currentMultiplications++;
				}
                
				
				maxCoef += GET_COEF(current);
				current = A.peek();
			}
			else {
				break;
			}
			
		}
        
        if (maxCoef % p == 0) {
            continue;
        }
        
        	
        setCoef(max, maxCoef % p);
        result.push_back(max);
	}
		

	cout << "Starting with last set: Extracting remaining elements..." << endl;
	
	// extract all remaining elements from heap
	while (!A.isEmpty()) {
		monom_t max = A.poll();
        		
		coef_t maxCoef = GET_COEF(max);
		deg_t maxDegree = GET_DEGREE(max);
		
		monom_t current = A.peek();
		while (current != 0) {
			if (GET_DEGREE(current) == maxDegree) {				
				current = A.poll();
				maxCoef += GET_COEF(current);
                
				current = A.peek();
			}
			else {
				break;
			}
			
		}
        
        if (maxCoef % p == 0) {
            continue;
        }
        
        setCoef(max, maxCoef % p);
        result.push_back(max);
	}
}



// define the number of pairs that seperate between the use of the 
// Funnel Heap over the Binary Heap. If the number of pairs is less
// than CUTOFF_LINE, then the multiplication will be done using the 
// Binary Heap, and if not, the Funnel Heap will be used
#define CUTOFF_LINE 1000		// chosen based on test results (Note: future improvements required)

void heapChooser (size_t numOfPairs, size_t heap_cap) {
	if (numOfPairs < CUTOFF_LINE) {
		cout << "Using Binary Heap" << endl;
		#define USE_BINARY_HEAP
	}
	else {
		cout << "Using Funnel Heap" << endl;
		#define USE_FUNNEL_HEAP
	}
}

// returns the degree of the polynomial
deg_t deg(const poly_t &p) {
	return GET_DEGREE(p[0]);
}


// parse the polynomial string into a poly_t
void parsePolynomialString (string p, poly_t &result) {
    
    long res_size = getNumberOfMonomials(p.c_str());
    p.append("?");      // ? is a sentinel value
    
	// reserve the size for the result
   	int resultPosition = 0;    
	
	string num = "";
    
    coef_t coef = 0;
    deg_t degree = 0;
	
	size_t i = 0;               // to traverse the poly string]
    char current = p[i++];
    skipSpaces(current, i, p);
    
	while (current != '?') {
        if (current == 'x') {
            coef = 1;
            
            current = p[i++];
            
            if (current == '^') {
                current = p[i++];       // current is (start of) a number
                
                while (current >= '0' && current <= '9') {
                    //num.append(current + "");
                    num += current;
                    current = p[i++];
                }
                
                degree = atoll(num.c_str());
                num.clear();
            }
            else {      // degree is 1
                degree = 1;
            }
            
            skipSpaces(current, i, p);   // go to nearest '+' or end
        }
        else {      // current is a number
            num.clear();
            while (current >= '0' && current <= '9') {
                //num.append(current + "");
                num += current;
                current = p[i++];
                
            }
            
            coef = atoll(num.c_str());
            num.clear();
            
            // coef read; check for degree
            
            if (current == '*') {           // degree exists
                current = p[i++];           // current = x
                
                current = p[i++];           // current is ^ or end of monomial
                
                if (current == '^') {       
                    current = p[i++];       // current is (start of) a number
                    
                    while (current >= '0' && current <= '9') {
                        //num.append(current + "");
                        num += current;
                        current = p[i++];
                    }
                    
                    degree = atoll(num.c_str());
                    num.clear();
                }
                else {      // degree is 1
                    degree = 1;
                }
                
            }
            else {              // degree is 0
                degree = 0;
            }
            
            skipSpaces(current, i, p);
        }
        
		result.push_back(createMonomial(degree, coef, 0));
        
        
        if (current == '?') {       // done reading poly
            break;
        }
        else {      // current is '+'
            current = p[i++];
            skipSpaces(current, i, p);
        }
	}
}


inline void skipSpaces (char &current, size_t &index, string line) {
    while (index <= line.length() && current == ' ') {	
        current = line[index++];
    }
}



/*
 getNumberOfMonomials: gets the number of monomials in a (SORTED) encoded polynomial
 */
int getNumberOfMonomials (const char *f) {
	const char *current = f;
	
	int numberOfMonomails = 1;
	while ((*current) != '\0') {
		if ((*current) == '+') {		// found an exponent
			numberOfMonomails++;
		}
		
		current++;
	}
	
	return numberOfMonomails;
}

size_t polynomialSize(const poly_t &p) {
    return p.size();
   // return GET_DEGREE(p[0]);
}



/*
 *  getMaxFromPair: Updates the description array with information about
 *                  the max element in a specific pair. The description 
 *                  array will contain the following information:
 *                      a) At index 0: the degree of the max element
 *                      b  At index 1: the coefficient of the max element
 *                      c) At index 2: the number of the pair in which it exists
 *                      d) At index 3: the index of the monomial in f
 *                      e) At index 4: the index of the monomial in g
 *
 *
 *  @param pairNumber:  the number of the pair from which the max is being 
 *                      retrieved. Note, 0 <= pairNumber < n, where n is the
 *                      total number of pairs.
 *
 *  @param g_i: an array containing the index of the next g-monomial to be multiplied 
 *              with its corresponding f-monomial. The index of the f-monomial that each
 *              g-index corresponds to is based on the index of that g-index. In other words,
 *              if c is a g-index located in the i'th position in the g_i array, then the
 *              next multiplication to be done is f_i * g_c.
 *
 *  @param description: an array (of size 5) that will be updated to contain a description
 *                      of the max element (as described above). Caution: the description
 *                      array should be allocated (with size 5) before being sent as a
 *                      parameter to this function, or else the behavior is undefined.
 *
 *  @param f_polynomials: a vector of the f-polynomials
 *
 *  @param g_polynomials: a vector of the g-polynomials
 */

inline void getMaxFromPair(size_t pairNumber, int g_i [], monom_t description [], vector<poly_t*> &f_polynomials, vector<poly_t*> &g_polynomials) {
    
    // loop over the multiplications of each f-monomial with its corresponding
    // g-monomial (identified by the g_i index array) in the specified pair
    // (stated by 'pairNumber') , and retrieve the element with the max degree, 
    // noting its description
    
    // get the specified pair of f_i and g_i polynomials
    poly_t* f_poly = f_polynomials[pairNumber]; 
    poly_t* g_poly = g_polynomials[pairNumber];
    
    
    monom_t maxDegree = 0;
    monom_t tempDegree = 0;
    monom_t tempCoef = 0;
    
    monom_t t = polynomialSize(*f_poly);
    // loop on all products of the monomials in the current pair, and find the max
    for (int i = 0; i < polynomialSize(*f_poly); i++) {
        if (g_i[i] != -1) {
            tempDegree = GET_DEGREE(f_poly->operator[](i)) + GET_DEGREE(g_poly->operator[](g_i[i]));
            tempCoef = GET_COEF(f_poly->operator[](i)) * GET_COEF(g_poly->operator[](g_i[i]));
            
            // if we found a new max element, update the description array
            if (tempDegree >= maxDegree) {
                maxDegree = tempDegree;
                
                description[0] = maxDegree;     // the value of the degree
                description[1] = tempCoef;       // the value of the coefficient
                description[2] = pairNumber;    // the number of the product pair
                description[3] = i;             // the number of the f-monomial
                description[4] = g_i[i];        // the number of the g-monomial
            }
        }
    }
    
    
}


/*
 *  getMaxFromAllPairs: Updates the description array with information about
 *                      the max element in a ALL pairs. The description 
 *                      array will contain the following information:
 *                          a) At index 0: the degree of the max element
 *                          b  At index 1: the coefficient of the max element
 *                          c) At index 2: the number of the pair in which it exists
 *                          d) At index 3: the index of the monomial in f
 *                          e) At index 4: the index of the monomial in g
 *
 *
 *  @param g_i_s:   a 2D array containing the the g_i array of each pair. For every f_i 
 *                  polynomial, the g_i array contains information on how to multiply the 
 *                  every monomial of f_i. Let c be a g-index located in the j'th position 
 *                  of the g_i array, then the next multiplication to be done is f_j * g_c.
 * 
 *
 *  @param description: an array (of size 5) that will be updated to contain a description
 *                      of the max element (as described above). Caution: the description
 *                      array should be allocated (with size 5) before being sent as a
 *                      parameter to this function, or else the behavior is undefined.
 *
 *  @param f_polynomials: a vector of the f-polynomials
 *
 *  @param g_polynomials: a vector of the g-polynomials
 */

void getMaxFromAllPairs (int ** &g_i_s, monom_t description [], vector<poly_t*> &f_polynomials, vector<poly_t*> &g_polynomials) {

    monom_t tempDescription [5]; // the array containing the description of a possible max
    
    
    // find the max element from all pairs
    for (size_t pair_index = 0; pair_index < f_polynomials.size(); pair_index++) {
        // get a new (possible) max
        getMaxFromPair(pair_index, g_i_s[pair_index], tempDescription, f_polynomials, g_polynomials);
        
        // now compare with previously found max, and make adjustments if necessary
        if (tempDescription[0] > description[0]) {
            
            // copy values of tempDescription to maxDescription
            description[0] = tempDescription[0];
            description[1] = tempDescription[1];
            description[2] = tempDescription[2];
            description[3] = tempDescription[3];
            description[4] = tempDescription[4];
        }
    }
    
}



void printPolynomial(const poly_t &p) {
    for (int i = 0; i < p.size(); i++) {
        
        if (i != 0) {
            printf(" + ");
        }
        

        if (GET_COEF(p[i]) != 0) {
            printf("%llu", GET_COEF(p[i]));
        }
        
        if (GET_DEGREE(p[i]) > 0) {
            printf("*x");
        }
        
        if (GET_DEGREE(p[i]) > 1) {
            printf("^%llu", GET_DEGREE(p[i]));
        }
    }
    
    printf("\n");
}


void toString(const poly_t &p, string &output) {
		
    stringstream ss (stringstream::in | stringstream::out);
        
    output = "";
    
    for (int i = 0; i < p.size(); i++) {
        
        if (i != 0) {
            ss << " + ";
        }
        
        
        if (GET_COEF(p[i]) != 0) {
            ss << GET_COEF(p[i]);
        }
        
        if (GET_DEGREE(p[i]) > 0) {
            ss << "*x";
        }
        
        if (GET_DEGREE(p[i]) > 1) {
            ss << "^"  << GET_DEGREE(p[i]);
        }
    }
    
    output = ss.str();
}
