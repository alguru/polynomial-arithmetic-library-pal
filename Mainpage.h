/*! \mainpage PAL Documentation
 *
 * \section intro_sec Introduction
 *
 * This manual documents the the PAL API.
 *
 * The high-level API documented in this manual provides an optimized solution for
 * computing a formula of the form
 * \f[
 *		\sum_{i = 1}^n (f_i * g_i)
 * \f]
 * where \f$ f_i \f$ and \f$ g_i \f$ are polynomials.
 * 
 *
 * \section install_sec Access
 *
 * Accessing the library is done through 3 simple steps, each defined more clearly in 
 * its corresponding section of the this manual.
 *
 * \subsection step1 Step1: Defining the calling environment (i.e. C++, MATLAB, etc..)
 * 
 * \subsection step2 Step2: Setup the input.
 *
 * \subsection step3 Step3: Call the library functions
 *
 */